package com.crfg.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbFactory {
	Connection connection;
	public Connection connectionMySQL(){
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost/crfg","root","kupra");
			connection.setAutoCommit(false);
			
			}
		
		catch(SQLException se){
		System.out.print(se);
		}
		
		catch(ClassNotFoundException ce){
			System.out.print(ce);
			}
		return connection;
		}
	public void closeMySQLCon(){
		if(connection!=null){
			try{
				connection.close();
			}
		catch(SQLException se){
			System.out.print(se);
		}
			
		}
		}
}
