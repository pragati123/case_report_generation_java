package com.crfg.model;

public class AppointmentDetail {
	int appointmentSrNo;
	int appointmentPatientId;
	String appointmentSpecialization;
	String appointmentDoctorName;
	int appointmentDoctorId;
	String appointmentDate;
	String appointmentTime;
	public int getAppointmentSrNo() {
		return appointmentSrNo;
	}
	public void setAppointmentSrNo(int appointmentSrNo) {
		this.appointmentSrNo = appointmentSrNo;
	}
	public int getAppointmentPatientId() {
		return appointmentPatientId;
	}
	public void setAppointmentPatientId(int appointmentPatientId) {
		this.appointmentPatientId = appointmentPatientId;
	}
	public String getAppointmentSpecialization() {
		return appointmentSpecialization;
	}
	public void setAppointmentSpecialization(String appointmentSpecialization) {
		this.appointmentSpecialization = appointmentSpecialization;
	}
	public String getAppointmentDoctorName() {
		return appointmentDoctorName;
	}
	public void setAppointmentDoctorName(String appointmentDoctorName) {
		this.appointmentDoctorName = appointmentDoctorName;
	}
	public int getAppointmentDoctorId() {
		return appointmentDoctorId;
	}
	public void setAppointmentDoctorId(int appointmentDoctorId) {
		this.appointmentDoctorId = appointmentDoctorId;
	}
	public String getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getAppointmentTime() {
		return appointmentTime;
	}
	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}

}
