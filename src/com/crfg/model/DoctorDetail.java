package com.crfg.model;

public class DoctorDetail {
	
	int doctorId;
	String doctorName;
	String doctorQualification;
	String doctorSpecialization;
	String doctorWorkExperience;
	float doctorFees;
	String doctorAddress;
	long doctorContact;
	String doctorTiming;
	String doctorUsername;
	String doctorPassword;
	String doctorEmail;
	public int getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getDoctorQualification() {
		return doctorQualification;
	}
	public void setDoctorQualification(String doctorQualification) {
		this.doctorQualification = doctorQualification;
	}
	public String getDoctorSpecialization() {
		return doctorSpecialization;
	}
	public void setDoctorSpecialization(String doctorSpecialization) {
		this.doctorSpecialization = doctorSpecialization;
	}
	public String getDoctorWorkExperience() {
		return doctorWorkExperience;
	}
	public void setDoctorWorkExperience(String doctorWorkExperience) {
		this.doctorWorkExperience = doctorWorkExperience;
	}
	public float getDoctorFees() {
		return doctorFees;
	}
	public void setDoctorFees(float doctorFees) {
		this.doctorFees = doctorFees;
	}
	public String getDoctorAddress() {
		return doctorAddress;
	}
	public void setDoctorAddress(String doctorAddress) {
		this.doctorAddress = doctorAddress;
	}
	public long getDoctorContact() {
		return doctorContact;
	}
	public void setDoctorContact(long doctorContact) {
		this.doctorContact = doctorContact;
	}
	public String getDoctorTiming() {
		return doctorTiming;
	}
	public void setDoctorTiming(String doctorTiming) {
		this.doctorTiming = doctorTiming;
	}
	public String getDoctorUsername() {
		return doctorUsername;
	}
	public void setDoctorUsername(String doctorUsername) {
		this.doctorUsername = doctorUsername;
	}
	public String getDoctorPassword() {
		return doctorPassword;
	}
	public void setDoctorPassword(String doctorPassword) {
		this.doctorPassword = doctorPassword;
	}
	public String getDoctorEmail() {
		return doctorEmail;
	}
	public void setDoctorEmail(String doctorEmail) {
		this.doctorEmail = doctorEmail;
	}

}
