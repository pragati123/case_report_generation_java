package com.crfg.model;

public class ReportDetail {
	int reportSrNo;
	int reportDoctorId;
	int reportPatientId;
	String patientName;
	String reportDate;
	String reportType;
	float reportFees;
	float reportHeight;
	float reportWeight;
	String reportBloodGroup;
	float reportHaemoglobin;
	float reportWBC;
	float reportRBC;
	float reportPlatelet;
	float reportTemperatureF;
	String reportVision;
	float reportBloodSystolic;
	float reportBloodDiastolic;
	int reportRespiratoryRate;
	String reportResult;
	public int getReportSrNo() {
		return reportSrNo;
	}
	public void setReportSrNo(int reportSrNo) {
		this.reportSrNo = reportSrNo;
	}
	public int getReportDoctorId() {
		return reportDoctorId;
	}
	public void setReportDoctorId(int reportDoctorId) {
		this.reportDoctorId = reportDoctorId;
	}
	public int getReportPatientId() {
		return reportPatientId;
	}
	public void setReportPatientId(int reportPatientId) {
		this.reportPatientId = reportPatientId;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public float getReportFees() {
		return reportFees;
	}
	public void setReportFees(float reportFees) {
		this.reportFees = reportFees;
	}
	public float getReportHeight() {
		return reportHeight;
	}
	public void setReportHeight(float reportHeight) {
		this.reportHeight = reportHeight;
	}
	public float getReportWeight() {
		return reportWeight;
	}
	public void setReportWeight(float reportWeight) {
		this.reportWeight = reportWeight;
	}
	public String getReportBloodGroup() {
		return reportBloodGroup;
	}
	public void setReportBloodGroup(String reportBloodGroup) {
		this.reportBloodGroup = reportBloodGroup;
	}
	public float getReportHaemoglobin() {
		return reportHaemoglobin;
	}
	public void setReportHaemoglobin(float reportHaemoglobin) {
		this.reportHaemoglobin = reportHaemoglobin;
	}
	public float getReportWBC() {
		return reportWBC;
	}
	public void setReportWBC(float reportWBC) {
		this.reportWBC = reportWBC;
	}
	public float getReportRBC() {
		return reportRBC;
	}
	public void setReportRBC(float reportRBC) {
		this.reportRBC = reportRBC;
	}
	public float getReportPlatelet() {
		return reportPlatelet;
	}
	public void setReportPlatelet(float reportPlatelet) {
		this.reportPlatelet = reportPlatelet;
	}
	public float getReportTemperatureF() {
		return reportTemperatureF;
	}
	public void setReportTemperatureF(float reportTemperatureF) {
		this.reportTemperatureF = reportTemperatureF;
	}
	public String getReportVision() {
		return reportVision;
	}
	public void setReportVision(String reportVision) {
		this.reportVision = reportVision;
	}
	public float getReportBloodSystolic() {
		return reportBloodSystolic;
	}
	public void setReportBloodSystolic(float reportBloodSystolic) {
		this.reportBloodSystolic = reportBloodSystolic;
	}
	public float getReportBloodDiastolic() {
		return reportBloodDiastolic;
	}
	public void setReportBloodDiastolic(float reportBloodDiastolic) {
		this.reportBloodDiastolic = reportBloodDiastolic;
	}
	public int getReportRespiratoryRate() {
		return reportRespiratoryRate;
	}
	public void setReportRespiratoryRate(int reportRespiratoryRate) {
		this.reportRespiratoryRate = reportRespiratoryRate;
	}
	public String getReportResult() {
		return reportResult;
	}
	public void setReportResult(String reportResult) {
		this.reportResult = reportResult;
	}

}
