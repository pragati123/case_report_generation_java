package com.crfg.model;

public class FeedbackDetail {
	
	int feedbackSerial,feedbackId;
	String feedbackRole,feedback;
	
	
	public int getFeedbackSerial() {
		return feedbackSerial;
	}
	public String getFeedbackRole() {
		return feedbackRole;
	}
	public void setFeedbackRole(String feedbackRole) {
		this.feedbackRole = feedbackRole;
	}
	
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public int getFeedbackId() {
		return feedbackId;
	}
	public void setFeedbackId(int feedbackId) {
		this.feedbackId = feedbackId;
	}

}
