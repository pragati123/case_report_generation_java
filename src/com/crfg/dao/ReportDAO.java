package com.crfg.dao;

import java.sql.Connection;
//import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.JOptionPane;

import com.crfg.factory.DbFactory;
import com.crfg.model.ReportDetail;
//import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.JOptionPane;

import com.crfg.factory.DbFactory;
import com.crfg.model.ReportDetail;

public class ReportDAO {
	
	DbFactory db ;
	PreparedStatement prepareStatement;
	Connection connection;
	int  result;
	ReportDetail reportDetail;
	
	public ReportDAO(){
		db = new DbFactory();
		connection = db.connectionMySQL();
		}
	public int addReport(ReportDetail reportDetail) throws ParseException{
		try {
			prepareStatement = connection.prepareStatement("insert into crfg_report_tab values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			prepareStatement.setInt(1,reportDetail.getReportSrNo());
			prepareStatement.setInt(2,reportDetail.getReportDoctorId());
			prepareStatement.setInt(3,reportDetail.getReportPatientId());
			prepareStatement.setString(4,reportDetail.getPatientName());
			prepareStatement.setString(5,reportDetail.getReportDate());				
			prepareStatement.setString(6,reportDetail.getReportType());
			prepareStatement.setFloat(7,reportDetail.getReportFees());
			prepareStatement.setFloat(8,reportDetail.getReportHeight());
			prepareStatement.setFloat(9,reportDetail.getReportWeight());
			prepareStatement.setString(10,reportDetail.getReportBloodGroup());
			prepareStatement.setFloat(11,reportDetail.getReportHaemoglobin());
			prepareStatement.setFloat(12,reportDetail.getReportWBC());
			prepareStatement.setFloat(13,reportDetail.getReportRBC());
			prepareStatement.setFloat(14,reportDetail.getReportPlatelet());
			prepareStatement.setFloat(15,reportDetail.getReportTemperatureF());
			prepareStatement.setString(16,reportDetail.getReportVision());
			prepareStatement.setFloat(17,reportDetail.getReportBloodSystolic());
			prepareStatement.setFloat(18,reportDetail.getReportBloodDiastolic());
			prepareStatement.setFloat(19,reportDetail.getReportRespiratoryRate());
			prepareStatement.setString(20,reportDetail.getReportResult());
			
			result= prepareStatement.executeUpdate();
			if(result>0)
			{connection.commit();
			}
}
		
		catch(SQLException se)
{
			JOptionPane.showMessageDialog(null,se);
			}
		
		return result;
		}

}
