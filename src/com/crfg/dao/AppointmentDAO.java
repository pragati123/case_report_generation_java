package com.crfg.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import java.sql.ResultSet;

import com.crfg.factory.DbFactory;
import com.crfg.model.AppointmentDetail;
import com.crfg.model.DoctorDetail;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import java.sql.ResultSet;

import com.crfg.factory.DbFactory;
import com.crfg.model.AppointmentDetail;
import com.crfg.model.DoctorDetail;

public class AppointmentDAO {
	 DbFactory db ;
	    PreparedStatement preparestatement;
	    Connection connection;
	    int  resultset;
	    ResultSet resultSet;
		public AppointmentDetail appointmentdetail;
		public DoctorDetail doctordetail;
	    public AppointmentDAO(){
	        db = new DbFactory();
	        connection = db.connectionMySQL();
	        
	        }
	    public int addAppointment(AppointmentDetail appointmentdetail){
	    	try{
	    		preparestatement = connection.prepareStatement("insert into crfg_appointment_tab values(?,?,?,?,?,?,?)");
	    		preparestatement.setInt(1,appointmentdetail.getAppointmentSrNo());
	    		preparestatement.setInt(2,appointmentdetail.getAppointmentPatientId());
	    		preparestatement.setString(3,appointmentdetail.getAppointmentSpecialization());
	    		preparestatement.setString(4,appointmentdetail.getAppointmentDoctorName());
	    		preparestatement.setInt(5,appointmentdetail.getAppointmentDoctorId());
	    		preparestatement.setString(6,appointmentdetail.getAppointmentDate());
	    		preparestatement.setString(7,appointmentdetail.getAppointmentTime());
	            
	            resultset=    preparestatement.executeUpdate();	           
	            if(resultset>0)
	            {
	                connection.commit();
	            }
	           }
	        
	        catch(SQLException ae)
	       {
	        	JOptionPane.showMessageDialog(null,ae);	   
	        	}
	            return resultset;
	           
	    	}

}
