package com.crfg.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.crfg.factory.DbFactory;
import com.crfg.model.DoctorDetail;

import net.proteanit.sql.DbUtils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.crfg.factory.DbFactory;
import com.crfg.model.DoctorDetail;

import net.proteanit.sql.DbUtils;

public class AdminDAO {
	
	DbFactory db ;
	PreparedStatement prepareStatement;
	Connection connection;
	int  result;
	int count=0;
	ResultSet resultSet;
	JTextField tfUsername;
	JPasswordField pfPassword;
	
	public AdminDAO(){
		db = new DbFactory();
		connection = db.connectionMySQL();
		}
	
	public boolean validateAdmin(String username, String passwordString){
		try{
			
			prepareStatement = connection.prepareStatement("Select admin_username,admin_password from crfg_admin_tab where admin_username = ? and admin_password = ?");
			prepareStatement.setString(1, username);
			prepareStatement.setString(2, passwordString);
			resultSet=prepareStatement.executeQuery();

			while(resultSet.next())
			{
				count++;
			}		
			
		}
		catch(SQLException e1){
			JOptionPane.showMessageDialog(null,e1);		
			}
			if(count == 1)
			{
				return true;
				
			}
			else
			{
				return false;
			}
	}
	
	public DoctorDetail viewDoctorDetail(int doctorId) {

		DoctorDetail doctorDetail = new DoctorDetail();
    	
		try{
			
			prepareStatement=connection.prepareStatement("select * from crfg_doctor_tab where doctor_id= ?");
			prepareStatement.setInt(1,doctorId);
			resultSet = prepareStatement.executeQuery();
			if(resultSet.next())
			{
				doctorDetail.setDoctorName(resultSet.getString(2));
				doctorDetail.setDoctorQualification(resultSet.getString(3));
				doctorDetail.setDoctorSpecialization(resultSet.getString(4));
				doctorDetail.setDoctorWorkExperience(resultSet.getString(5));
				doctorDetail.setDoctorFees(resultSet.getFloat(6));
				doctorDetail.setDoctorAddress(resultSet.getString(7));
				doctorDetail.setDoctorContact(resultSet.getInt(8));
				doctorDetail.setDoctorTiming(resultSet.getString(9));
				doctorDetail.setDoctorUsername(resultSet.getString(10));
				doctorDetail.setDoctorPassword(resultSet.getString(11));
				doctorDetail.setDoctorEmail(resultSet.getString(12));
			}
		}
		
		catch(SQLException e1){
			JOptionPane.showMessageDialog(null,e1);		
			}
		return doctorDetail;
	}
	public void displayDoctor(JTable doctorTable) {
		try{// TODO Auto-generated method stub
		prepareStatement=connection.prepareStatement("select * from crfg_doctor_tab");
		resultSet=prepareStatement.executeQuery();
		doctorTable.setModel(DbUtils.resultSetToTableModel(resultSet));
	} catch(SQLException se) {
        // TODO Auto-generated catch block
		JOptionPane.showMessageDialog(null,se);  
		}
	}
		
		public void displayPatient(JTable patientTable) {
			try{// TODO Auto-generated method stub
			prepareStatement=connection.prepareStatement("select * from crfg_patient_tab");
			resultSet=prepareStatement.executeQuery();
			patientTable.setModel(DbUtils.resultSetToTableModel(resultSet));
		} catch(SQLException se) {
	        // TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,se);	   
			}
		}
			
			public void displayFeedback(JTable feedbackTable) {
				try{// TODO Auto-generated method stub
				prepareStatement=connection.prepareStatement("select * from crfg_feedback_tab");
				resultSet=prepareStatement.executeQuery();
				feedbackTable.setModel(DbUtils.resultSetToTableModel(resultSet));
			} catch(SQLException se) {
		        // TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,se);		  
				}			
	}
			public void deleteDoctorDetail(int doctorId) {
				try{// TODO Auto-generated method stub
					prepareStatement=connection.prepareStatement("delete from crfg_doctor_tab where doctor_id = ?");
					prepareStatement.setInt(1,doctorId);
					result=prepareStatement.executeUpdate();
					JOptionPane.showMessageDialog(null, "Selected Doctor's detail deleted...");
					if(result>0){
						connection.commit();
					}
					//doctorTable.removeRow();
				} catch(SQLException se) {
					
			        // TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,se);		
					}
				}

}
