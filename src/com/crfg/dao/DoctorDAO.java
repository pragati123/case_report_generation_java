package com.crfg.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import com.crfg.factory.DbFactory;
import com.crfg.model.DoctorDetail;

import net.proteanit.sql.DbUtils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import com.crfg.factory.DbFactory;
import com.crfg.model.DoctorDetail;

import net.proteanit.sql.DbUtils;

public class DoctorDAO {
	
	DbFactory db;
	PreparedStatement prepareStatement;
	Connection connection;
	int result;
	int count;
	ResultSet resultSet;
	
	
	public DoctorDAO()
	{
		db=new DbFactory();
		connection=db.connectionMySQL();
		
	}
	
	public int addDoctor(DoctorDetail doctorDetail){
		try
		{
			prepareStatement=connection.prepareStatement("insert into crfg_doctor_tab values(?,?,?,?,?,?,?,?,?,?,?,?)");
			prepareStatement.setInt(1,doctorDetail.getDoctorId());
			prepareStatement.setString(2,doctorDetail.getDoctorName());
			prepareStatement.setString(3,doctorDetail.getDoctorQualification());
			prepareStatement.setString(4,doctorDetail.getDoctorSpecialization());
			prepareStatement.setString(5, doctorDetail.getDoctorWorkExperience());
			prepareStatement.setFloat(6,doctorDetail.getDoctorFees());
			prepareStatement.setString(7,doctorDetail.getDoctorAddress());
			prepareStatement.setLong(8, doctorDetail.getDoctorContact());
			prepareStatement.setString(9, doctorDetail.getDoctorTiming());
			prepareStatement.setString(10, doctorDetail.getDoctorUsername());
			prepareStatement.setString(11, doctorDetail.getDoctorPassword());
			prepareStatement.setString(12, doctorDetail.getDoctorEmail());
		
			result=prepareStatement.executeUpdate();
			if(result>0)
			{
				connection.commit();
				
			}
			}
		catch(SQLException ae)
		{
		JOptionPane.showMessageDialog(null, ae);
		}
	    return result;
	}
	
	public boolean validateDoctor(String username, String passwordString){
		try{
			
			prepareStatement = connection.prepareStatement("Select doctor_username,doctor_password from crfg_doctor_tab where doctor_username = ? and doctor_password = ?");
			prepareStatement.setString(1, username);
			prepareStatement.setString(2, passwordString);
			resultSet=prepareStatement.executeQuery();

			while(resultSet.next())
			{
				count++;
			}	
			}	
			catch(SQLException e1){
				JOptionPane.showMessageDialog(null, e1);
			}
		
			
			if(count == 1)
			{
				return true;
			}
			else
			{
				return false;
			}
			
			}
			
	
	public List<String> getDoctorName(String doctorSpecialization){
    	List<String> doctorNames=new ArrayList<String>();
        try{ 
        	
            prepareStatement = connection.prepareStatement("select doctor_name from crfg_doctor_tab where doctor_specialization= ?");
            prepareStatement.setString(1,doctorSpecialization);
            resultSet=prepareStatement.executeQuery();
               
                    while(resultSet.next())
                    {
                    	doctorNames.add(resultSet.getString(1));
                    	connection.commit();  
                    	}

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        }
       
        return doctorNames; 
    }
	
	public  int getDoctorId(String doctorName){
		int doctorId=0 ;
    	 try{ 
        	
            prepareStatement = connection.prepareStatement("select doctor_id from crfg_doctor_tab where doctor_name= ?");
           prepareStatement.setString(1,doctorName);
            resultSet=prepareStatement.executeQuery();
               
            if(resultSet.next()){
    			doctorId = resultSet.getInt(1);
    		}

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        }
    	
        return doctorId;      
	}
	
	public int updateDoctorName(String doctorName, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_name =? where doctor_id= ?");
            prepareStatement.setString(1,doctorName);
            prepareStatement.setInt(2,doctorId);
            result = prepareStatement.executeUpdate();
            //JOptionPane.showMessageDialog(null, "Data Updated Successfully...");

                    if(result>0)
                    {connection.commit();

        }
                    }catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);  
        	}
		
       return result;
        }
	public int updateDoctorQualification(String doctorQualification, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_qualification =? where doctor_id= ?");
            prepareStatement.setString(1,doctorQualification);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                    }
                    

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	public int updateDoctorSpecialization(String doctorSpecialization, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_specialization =? where doctor_id= ?");
            prepareStatement.setString(1,doctorSpecialization);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();

        }
                    } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	public int updateDoctorWorkExperience(String doctorWorkExperience, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_work_experience =? where doctor_id= ?");
            prepareStatement.setString(1,doctorWorkExperience);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                    }
                    
        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	public int updateDoctorFees(float doctorFees, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_fees =? where doctor_id= ?");
            prepareStatement.setFloat(1, doctorFees);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                     }

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	public int updateDoctorAddress(String doctorAddress, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_address =? where doctor_id= ?");
            prepareStatement.setString(1,doctorAddress);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                        }

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	public int updateDoctorContact(long doctorContact, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_contact=? where doctor_id= ?");
            prepareStatement.setLong(1,doctorContact);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                      }

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	public int updateDoctorTiming(String doctorTiming, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_timing =? where doctor_id= ?");
            prepareStatement.setString(1,doctorTiming);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                       }

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }
	
	public int updateDoctorEmail(String doctorEmail, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_email =? where doctor_id= ?");
            prepareStatement.setString(1,doctorEmail);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                        }

        } catch (SQLException se) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,se);
        	}
		
       return result;
       }
	
	public int updateDoctorUsername(String doctorUsername, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_username =? where doctor_id= ?");
            prepareStatement.setString(1,doctorUsername);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                       }

        } catch (SQLException we) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,we);
        	}
		
       return result;
        }

	public int updateDoctorPassword(String doctorPassword, int doctorId){
		try{
            prepareStatement = connection.prepareStatement("update crfg_doctor_tab set doctor_password =? where doctor_id= ?");
            prepareStatement.setString(1,doctorPassword);
            prepareStatement.setInt(2,doctorId);
            result=prepareStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
                    if(result>0)
                    {connection.commit();
                    
                  }

        } catch (SQLException se) {
            // TODO Auto-generated catch block
        	JOptionPane.showMessageDialog(null,se);  
        	}
		
       return result;
}
	public void deleteReport(int serialNumber) {
			try{
				prepareStatement=connection.prepareStatement("delete from crfg_report_tab where report_sr_no = ?");
				prepareStatement.setInt(1,serialNumber);
				result=prepareStatement.executeUpdate();
				JOptionPane.showMessageDialog(null, "Data Updated Successfully...");
				if(result>0){
					connection.commit();
					
				}
			}
		 catch(SQLException se){
			 JOptionPane.showMessageDialog(null,se);	
			 }
			
			}
	 
	public void viewAppointment(JTable appointmentTable, int doctorId) {

		try {
			
			prepareStatement = connection.prepareStatement("Select * from crfg_appointment_tab where appointment_doctor_id=?");
			prepareStatement.setInt(1,doctorId);
			resultSet=prepareStatement.executeQuery();
			appointmentTable.setModel(DbUtils.resultSetToTableModel(resultSet));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e);		
			}
		
		
	}

	public void viewReport(JTable reportTable,int doctorId) {
		
		try {
			
			prepareStatement = connection.prepareStatement("Select * from crfg_report_tab where report_doctor_id=?");
			prepareStatement.setInt(1,doctorId);
			resultSet=prepareStatement.executeQuery();
			reportTable.setModel(DbUtils.resultSetToTableModel(resultSet));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e);	
			}
		
	}

	public void deleteAppointment(int serialNumber) {
		try{
		prepareStatement=connection.prepareStatement("delete from crfg_appointment_tab where appointment_sr_no = ?");
		prepareStatement.setInt(1,serialNumber);
		result=prepareStatement.executeUpdate();
		JOptionPane.showMessageDialog(null, "Selected Appointment deleted...");
		if(result>0){
			connection.commit();
		}
	}catch(SQLException se){
		JOptionPane.showMessageDialog(null,se);		
	}
		
	}

}
