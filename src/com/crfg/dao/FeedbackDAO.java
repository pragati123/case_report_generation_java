package com.crfg.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.crfg.factory.DbFactory;
import com.crfg.model.FeedbackDetail;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.crfg.factory.DbFactory;
import com.crfg.model.FeedbackDetail;

public class FeedbackDAO {
	
	DbFactory db;
	PreparedStatement ps;
	Connection con;
	int res;
	
	
	public FeedbackDAO()
	{
		db=new DbFactory();
		con=db.connectionMySQL();
		
	}
	
	public int addFeedback(FeedbackDetail feedbackDetail){
		try
		{
		ps=con.prepareStatement("insert into crfg_feedback_tab values(?,?,?,?)");
		ps.setInt(1,feedbackDetail.getFeedbackSerial());
		ps.setString(2,feedbackDetail.getFeedbackRole());
		ps.setInt(3,feedbackDetail.getFeedbackId());
		ps.setString(4,feedbackDetail.getFeedback());
		
			res=ps.executeUpdate();
			if(res>0)
			{
				con.commit();
				
			}
			}
		catch(SQLException ae)
		{
			JOptionPane.showMessageDialog(null,ae);		}
		
	    return res;
	}

}
