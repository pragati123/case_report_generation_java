package com.crfg.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import com.crfg.factory.DbFactory;
import com.crfg.model.DoctorDetail;
import com.crfg.model.PatientDetail;

import net.proteanit.sql.DbUtils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import com.crfg.factory.DbFactory;
import com.crfg.model.DoctorDetail;
import com.crfg.model.PatientDetail;

import net.proteanit.sql.DbUtils;

public class PatientDAO {
	
	 DbFactory db ;
	    PreparedStatement prepareStatement;
	    Connection connection;
	    int result;
	    int count;
	    ResultSet  resultSet;
		public PatientDetail patientdetail;
	    public PatientDAO(){
	        db = new DbFactory();
	        connection = db.connectionMySQL();
	        
	        }
	    public int addpatient(PatientDetail patientdetail){
	    	try{
	    		prepareStatement = connection.prepareStatement("insert into crfg_patient_tab values(?,?,?,?,?,?,?,?,?)");
	    		prepareStatement.setInt(1,patientdetail.getPatientId());
	    		prepareStatement.setString(2,patientdetail.getPatientName());
	    		prepareStatement.setInt(3,patientdetail.getPatientAge());
	    		prepareStatement.setString(4,patientdetail.getPatientGender());
	    		prepareStatement.setString(5,patientdetail.getPatientAddress());
	    		prepareStatement.setLong(6,patientdetail.getPatientContact());
	    		prepareStatement.setString(7,patientdetail.getPatientEmail());
	    		prepareStatement.setString(8,patientdetail.getPatientUsername());
	    		prepareStatement.setString(9,patientdetail.getPatientPassword());
	            
	    		result = prepareStatement.executeUpdate();
	           
	            if(result>0)
	            {
	            	connection.commit();
	            }
	           }
	        
	        catch(SQLException ae)
	       {
	        	JOptionPane.showMessageDialog(null,ae); 
	        	}
	    	
	            return result;
	           
	    	}
	    
	    public boolean validatePatient(String username, String passwordString){
			try{
				
				prepareStatement = connection.prepareStatement("Select patient_username,patient_password from crfg_patient_tab where patient_username = ? and patient_password = ?");
				prepareStatement.setString(1, username);
				prepareStatement.setString(2, passwordString);
				resultSet=prepareStatement.executeQuery();

				while(resultSet.next())
				{
					count++;
				}			
				
			}catch(SQLException e1){
				JOptionPane.showMessageDialog(null,e1);
			}

				if(count == 1)
				{
					return true;			
				}
				else
				{
					return false;
				}
	}
			
			public int updatePatientName(String patientName, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_name =? where patient_id= ?");
		            prepareStatement.setString(1,patientName);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result > 0)
		                    {connection.commit();
		                    }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);	     
		        	}
				
		       return result;
		        }
			
			public int updatePatientAge(int patientAge, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_age =? where patient_id= ?");
		            prepareStatement.setInt(1,patientAge);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                  }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);	
		        	}
				
		       return result;
		        }
			
		    public int updatePatientAddress(String patientAddress, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_address =? where patient_id= ?");
		            prepareStatement.setString(1,patientAddress);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                      }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);
		        }
				
		       return result;
		        }
		    
		    public int updatePatientGender(String patientGender, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_gender =? where patient_id= ?");
		            prepareStatement.setString(1,patientGender);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                     }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);
		        }
				
		       return result;
		        }
		    public int updatePatientContact(int patientContact, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_contact =? where patient_id= ?");
		            prepareStatement.setInt(1,patientContact);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                     }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);
		        }
				
		       return result;
		        }
		    public int updatePatientEmail(String patientEmail, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_email =? where patient_id= ?");
		            prepareStatement.setString(1,patientEmail);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                     }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);
		        }
				
		       return result;
		        }
		    public int updatePatientUsername(String patientUsername, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_username =? where patient_id= ?");
		            prepareStatement.setString(1,patientUsername);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                   ;  
		                    }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);
		        }
				
		       return result;
		       }
		    public int updatePatientPassword(String password, int patientId){
				try{
		            prepareStatement = connection.prepareStatement("update crfg_patient_tab set patient_password =? where patient_id= ?");
		            prepareStatement.setString(1,password);
		            prepareStatement.setInt(2,patientId);
		            result=prepareStatement.executeUpdate();
		                
		                    if(result>0)
		                    {connection.commit();
		                   ;  
		                    }

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);
		        }
				
		       return result;
		       }
		    
		    public List<String> getDoctorNames(String doctorSpecialization){
		    	List<String> doctorNames=new ArrayList<String>();
		        try{ 
		        	
		            prepareStatement = connection.prepareStatement("select doctor_name from crfg_doctor_tab where doctor_specialization= ?");
		            prepareStatement.setString(1,doctorSpecialization);
		            resultSet=prepareStatement.executeQuery();
		               
		                    while(resultSet.next())
		                    {
		                    	doctorNames.add(resultSet.getString(1));
		                    	
		                    	connection.commit();   
		                    	}

		        } catch (SQLException we) {
		            // TODO Auto-generated catch block
		        	JOptionPane.showMessageDialog(null,we);	   
		        	}
		        
		        return doctorNames; 
		    }
		    
		    public DoctorDetail getDoctorDetail(String doctorName) {

				DoctorDetail doctorDetail = new DoctorDetail();
		    	
				try{
					
					prepareStatement=connection.prepareStatement("select * from crfg_doctor_tab where doctor_name= ?");
					prepareStatement.setString(1,doctorName);
					resultSet = prepareStatement.executeQuery();
					if(resultSet.next())
					{
						doctorDetail.setDoctorQualification(resultSet.getString(3));
						doctorDetail.setDoctorSpecialization(resultSet.getString(4));
						doctorDetail.setDoctorWorkExperience(resultSet.getString(5));
						doctorDetail.setDoctorFees(resultSet.getFloat(6));
						doctorDetail.setDoctorAddress(resultSet.getString(7));
						doctorDetail.setDoctorContact(resultSet.getInt(8));
						doctorDetail.setDoctorTiming(resultSet.getString(9));
						doctorDetail.setDoctorEmail(resultSet.getString(12));
					}
				}
				
				catch(SQLException e1){
					JOptionPane.showMessageDialog(null,e1);		
					}
			
				return doctorDetail;
			}
			public void viewGeneratedReport(JTable myReportsTable, int patientId) {
				try {
					
					prepareStatement = connection.prepareStatement("Select * from crfg_report_tab where report_patient_id=?");
					prepareStatement.setInt(1,patientId);
					resultSet=prepareStatement.executeQuery();
					myReportsTable.setModel(DbUtils.resultSetToTableModel(resultSet));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e);		
					}
				
			}

}
