package com.crfg.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.crfg.model.PatientDetail;
import com.crfg.service.PatientService;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.ButtonGroup;
import java.awt.Color;

@SuppressWarnings("serial")
public class PatientRegistration extends JFrame {

	private JPanel contentPane;
	private JTextField tfPatientName;
	private JTextField tfPatientAge;
	private JTextField tfPatientContact;
	private JTextField tfPatientEmail;
	private JTextField tfPatientUsername;
	private JPasswordField pfPatientPassword;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PatientRegistration frame = new PatientRegistration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public PatientRegistration() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 450, 495);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 153, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPatientRegistrationForm = new JLabel("Patient Registration Form");
		lblPatientRegistrationForm.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientRegistrationForm.setBounds(160, 11, 205, 32);
		contentPane.add(lblPatientRegistrationForm);
		
		JLabel lblPatientName = new JLabel("Patient Name");
		lblPatientName.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientName.setBounds(81, 57, 284, 14);
		contentPane.add(lblPatientName);
		
		JLabel lblPatientAge = new JLabel("Patient Age");
		lblPatientAge.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientAge.setBounds(81, 89, 284, 14);
		contentPane.add(lblPatientAge);
		
		JLabel lblPatientGender = new JLabel("Patient Gender");
		lblPatientGender.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientGender.setBounds(81, 127, 109, 14);
		contentPane.add(lblPatientGender);
		
		JLabel lblPatientAddress = new JLabel("Patient Address");
		lblPatientAddress.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientAddress.setBounds(81, 158, 284, 14);
		contentPane.add(lblPatientAddress);
		
		JLabel lblPatientContact = new JLabel("Patient Contact");
		lblPatientContact.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientContact.setBounds(81, 224, 284, 14);
		contentPane.add(lblPatientContact);
		
		JLabel lblPatientEmail = new JLabel("Email");
		lblPatientEmail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientEmail.setBounds(81, 260, 284, 14);
		contentPane.add(lblPatientEmail);
		
		JLabel lblPatientUsername = new JLabel("Username");
		lblPatientUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientUsername.setBounds(80, 294, 285, 14);
		contentPane.add(lblPatientUsername);
		
		JLabel lblPatientPassword = new JLabel("Password");
		lblPatientPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientPassword.setBounds(82, 325, 283, 14);
		contentPane.add(lblPatientPassword);
		
		tfPatientName = new JTextField();
		tfPatientName.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfPatientName.setColumns(10);
		tfPatientName.setBounds(236, 54, 129, 20);
		contentPane.add(tfPatientName);
		
		tfPatientAge = new JTextField();
		tfPatientAge.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfPatientAge.setColumns(10);
		tfPatientAge.setBounds(236, 86, 129, 20);
		contentPane.add(tfPatientAge);
		
		JRadioButton rbFemale = new JRadioButton("Female");
		rbFemale.setBackground(new Color(255, 255, 204));
		rbFemale.setForeground(new Color(0, 0, 0));
		rbFemale.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		buttonGroup.add(rbFemale);
		rbFemale.setBounds(193, 123, 109, 23);
		contentPane.add(rbFemale);
		
		JRadioButton rbMale = new JRadioButton("Male");
		rbMale.setBackground(new Color(255, 255, 204));
		rbMale.setForeground(new Color(0, 0, 0));
		rbMale.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		buttonGroup.add(rbMale);
		rbMale.setBounds(301, 123, 109, 23);
		contentPane.add(rbMale);
		
		JTextArea taPatientAddress = new JTextArea();
		taPatientAddress.setFont(new Font("Colonna MT", Font.PLAIN, 15));
		taPatientAddress.setBounds(236, 153, 118, 57);
		contentPane.add(taPatientAddress);
		
		tfPatientContact = new JTextField();
		tfPatientContact.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfPatientContact.setColumns(10);
		tfPatientContact.setBounds(236, 221, 129, 20);
		contentPane.add(tfPatientContact);
		
		tfPatientEmail = new JTextField();
		tfPatientEmail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfPatientEmail.setColumns(10);
		tfPatientEmail.setBounds(236, 257, 129, 20);
		contentPane.add(tfPatientEmail);
		
		tfPatientUsername = new JTextField();
		tfPatientUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfPatientUsername.setColumns(10);
		tfPatientUsername.setBounds(236, 291, 129, 20);
		contentPane.add(tfPatientUsername);
		
		pfPatientPassword = new JPasswordField();
		pfPatientPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		pfPatientPassword.setBounds(236, 322, 129, 20);
		contentPane.add(pfPatientPassword);
				
		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(164, 369, 89, 23);
		contentPane.add(btnRegister);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PatientLogin patientLogin=new PatientLogin();
				patientLogin.setVisible(true);
				dispose();
			}
		});
		btnBack.setBounds(25, 18, 89, 23);
		contentPane.add(btnBack);
		btnRegister.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				EmailValidator emailValidator = new EmailValidator();
				if(!emailValidator.validate(tfPatientEmail.getText().trim())){
					JOptionPane.showMessageDialog(btnRegister, "Invalid EmailId");
				}
				else{		
				String patientName=tfPatientName.getText();
				String patientAge=tfPatientAge.getText();
				String patientAddress=taPatientAddress.getText();
				String patientContact=tfPatientContact.getText();
				String patientEmail=tfPatientEmail.getText();
				String patientUsername=tfPatientUsername.getText();
				char[] patientPassword=pfPatientPassword.getPassword();
				String password=new String(patientPassword);
				String gender;
 				if(rbFemale.isSelected()){
 					gender="Male";
 				}
 				else
 					gender="Female";
 				
				try{
					PatientDetail patientDetail=new PatientDetail();
					
					patientDetail.setPatientName(patientName);
					patientDetail.setPatientAge(Integer.parseInt(patientAge));
					patientDetail.setPatientGender(gender);
					patientDetail.setPatientAddress(patientAddress);
					patientDetail.setPatientContact(Long.parseLong(patientContact));
					patientDetail.setPatientEmail(patientEmail);
					patientDetail.setPatientUsername(patientUsername);
					patientDetail.setPatientPassword(password);
					PatientService patientService=new PatientService();
					
					if(patientName.length()!=0 || patientAge.length()!=0 || gender.length()!=0 || patientAddress.length()!=0 || patientContact.length()!=0 || patientEmail.length()!=0 || patientUsername.length()!=0 || password.length()!=0)
					{
						if(password.length() > 6)
						{								
							if(patientContact.length() == 10)
							{
								int x=patientService.addPatientService(patientDetail);
								if(x>0)
								{
									JOptionPane.showMessageDialog(null, "Patient SuccessFully Registered");
								}
								PatientLogin patientLogin=new PatientLogin();
								patientLogin.setVisible(true);
								dispose();
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Invalid Conctact");
							}
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Weak Password. Password should conatin atleast 6 characters..");
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Please fill all the mandatory Fields");
					}	
					}
					catch(NumberFormatException ne)
					{
						JOptionPane.showMessageDialog(null,"Please fill all the mandatory Fields correctly");	
					}
				}}
		});
    }
}	
