package com.crfg.controller;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.crfg.model.DoctorDetail;
import com.crfg.service.DoctorService;
import javax.swing.JTextArea;
import java.awt.Color;

@SuppressWarnings("serial")

public class DoctorRegistration extends JFrame{
	
	private JPanel contentPane;
	private JTextField tfDoctorname;
	private JTextField tfDoctorQualification;
	private JTextField tfDoctorSpecialisation;
	private JTextField tfDoctorExperience;
	private JTextField tfDoctorFees;
	private JTextField tfDoctorContact;
	private JTextField tfDoctorTiming;
	private JTextField tfDoctorUsername;
	private JPasswordField pfDoctorPassword;
	private JTextField tfDoctorEmail;
	private JLabel lblDoctorRegistration;
	private JTextArea taDoctorAddress;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DoctorRegistration frame = new DoctorRegistration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DoctorRegistration() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 435, 504);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDoctorName = new JLabel("Name");
		lblDoctorName.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorName.setBounds(10, 54, 46, 14);
		contentPane.add(lblDoctorName);
		
		JLabel lblDoctorQualification = new JLabel("Qualification");
		lblDoctorQualification.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorQualification.setBounds(10, 85, 104, 14);
		contentPane.add(lblDoctorQualification);
		
		JLabel lblDoctorSpecialization = new JLabel("Specialisation");
		lblDoctorSpecialization.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorSpecialization.setBounds(10, 117, 104, 14);
		contentPane.add(lblDoctorSpecialization);
		
		JLabel lblDoctorWorkExperience = new JLabel("Work Experience");
		lblDoctorWorkExperience.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorWorkExperience.setBounds(10, 148, 129, 14);
		contentPane.add(lblDoctorWorkExperience);
		
		JLabel lblDoctorFees = new JLabel("Fees");
		lblDoctorFees.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorFees.setBounds(10, 177, 46, 14);
		contentPane.add(lblDoctorFees);
		
		JLabel lblDoctorAddress = new JLabel("Address");
		lblDoctorAddress.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorAddress.setBounds(10, 202, 86, 14);
		contentPane.add(lblDoctorAddress);
		
		JLabel lblDoctorContact = new JLabel("Contact");
		lblDoctorContact.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorContact.setBounds(10, 285, 86, 14);
		contentPane.add(lblDoctorContact);
		
		JLabel lblDoctorTiming = new JLabel("Timing");
		lblDoctorTiming.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorTiming.setBounds(10, 310, 86, 14);
		contentPane.add(lblDoctorTiming);
		
		JLabel lblDoctorUsername = new JLabel("Username");
		lblDoctorUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorUsername.setBounds(10, 341, 86, 14);
		contentPane.add(lblDoctorUsername);
		
		JLabel lblDoctorPassword = new JLabel("Password");
		lblDoctorPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorPassword.setBounds(10, 372, 86, 14);
		contentPane.add(lblDoctorPassword);
		
		JLabel lblDoctorEmail = new JLabel("Email");
		lblDoctorEmail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorEmail.setBounds(10, 397, 46, 14);
		contentPane.add(lblDoctorEmail);
		
		tfDoctorname = new JTextField();
		tfDoctorname.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorname.setColumns(10);
		tfDoctorname.setBounds(207, 52, 163, 20);
		contentPane.add(tfDoctorname);
		
		tfDoctorQualification = new JTextField();
		tfDoctorQualification.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorQualification.setColumns(10);
		tfDoctorQualification.setBounds(207, 83, 163, 20);
		contentPane.add(tfDoctorQualification);
		
		tfDoctorSpecialisation = new JTextField();
		tfDoctorSpecialisation.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorSpecialisation.setColumns(10);
		tfDoctorSpecialisation.setBounds(207, 115, 163, 20);
		contentPane.add(tfDoctorSpecialisation);
		
		tfDoctorExperience = new JTextField();
		tfDoctorExperience.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorExperience.setColumns(10);
		tfDoctorExperience.setBounds(207, 146, 163, 20);
		contentPane.add(tfDoctorExperience);
		
		tfDoctorFees = new JTextField();
		tfDoctorFees.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorFees.setColumns(10);
		tfDoctorFees.setBounds(207, 175, 163, 20);
		contentPane.add(tfDoctorFees);
		
		tfDoctorContact = new JTextField();
		tfDoctorContact.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorContact.setColumns(10);
		tfDoctorContact.setBounds(207, 283, 163, 20);
		contentPane.add(tfDoctorContact);
		
		tfDoctorTiming = new JTextField();
		tfDoctorTiming.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorTiming.setColumns(10);
		tfDoctorTiming.setBounds(207, 308, 163, 20);
		contentPane.add(tfDoctorTiming);
		
		tfDoctorUsername = new JTextField();
		tfDoctorUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorUsername.setColumns(10);
		tfDoctorUsername.setBounds(207, 339, 163, 20);
		contentPane.add(tfDoctorUsername);
		
		pfDoctorPassword = new JPasswordField();
		pfDoctorPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		pfDoctorPassword.setBounds(207, 370, 163, 20);
		contentPane.add(pfDoctorPassword);
		
		tfDoctorEmail = new JTextField();
		tfDoctorEmail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorEmail.setColumns(10);
		tfDoctorEmail.setBounds(207, 395, 163, 20);
		contentPane.add(tfDoctorEmail);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnRegister.setBounds(45, 432, 118, 23);
		contentPane.add(btnRegister);
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EmailValidator  emailValidator = new EmailValidator();
				if(!emailValidator.validate(tfDoctorEmail.getText().trim()) ){
					JOptionPane.showMessageDialog(btnRegister, "Invalid EmailId...");
				}
				else{
				try{
				String doctorName=tfDoctorname.getText();
				String doctorQualification = tfDoctorQualification.getText();
				String doctorSpecialisation = tfDoctorSpecialisation.getText();
				String doctorExperience = tfDoctorExperience.getText();
				String doctorFees = tfDoctorFees.getText();
				String doctorAddress = taDoctorAddress.getText();
				String doctorContact =tfDoctorContact.getText();
				String doctorTiming = tfDoctorTiming.getText();
				String doctorUsername = tfDoctorUsername.getText();
				
				DoctorDetail doctorDetail=new DoctorDetail();
				doctorDetail.setDoctorName(doctorName);
				doctorDetail.setDoctorQualification(doctorQualification);
				doctorDetail.setDoctorSpecialization(doctorSpecialisation);
				doctorDetail.setDoctorWorkExperience(doctorExperience);
				doctorDetail.setDoctorFees(Float.parseFloat(doctorFees));
				doctorDetail.setDoctorAddress(doctorAddress);
				doctorDetail.setDoctorContact(Long.parseLong(doctorContact));
				doctorDetail.setDoctorTiming(doctorTiming);
				doctorDetail.setDoctorUsername(doctorUsername);
				doctorDetail.setDoctorEmail(tfDoctorEmail.getText());
				
				char[] tempPassword = pfDoctorPassword.getPassword();
				String doctorPassword = new String(tempPassword); 
				doctorDetail.setDoctorPassword(doctorPassword);
				
				doctorDetail.setDoctorEmail(tfDoctorEmail.getText());
								
				DoctorService doctorService=new DoctorService();
				int x=doctorService.addDoctorService(doctorDetail);
				if(x>0){
					if(doctorName.length()==0 || doctorQualification.length()==0 || doctorSpecialisation.length()==0 || doctorExperience.length()==0 || doctorFees.length()==0 || doctorAddress.length()==0 || doctorContact.length()==0 || doctorTiming.length()==0 || doctorUsername.length()==0 || doctorPassword.length()==0){
						JOptionPane.showMessageDialog(null, "Please fill all the mandatory Fields");
					}
					else if(doctorPassword.length() < 6){
						JOptionPane.showMessageDialog(null, "Weak Password. Password should conatin atleast 6 characters..");
					}
					else if(doctorContact.length() != 10){
						JOptionPane.showMessageDialog(null, "Invalid Conctact");
					}
					else{
						JOptionPane.showMessageDialog(null, "Doctor SuccessFully Registered");

					}
				}
				
				DoctorLogin doctorLogin=new DoctorLogin();
				doctorLogin.setVisible(true);
				dispose();
			
				}catch(NumberFormatException ne){
					JOptionPane.showMessageDialog(null,"Please fill all the mandatory Fields correctly");
					
				}
				}
		
		}
			});
		
		
		JButton btnClear = new JButton("Clear");
		btnClear.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnClear.setBounds(173, 432, 89, 23);
		contentPane.add(btnClear);
		
		lblDoctorRegistration = new JLabel("Doctor Registration");
		lblDoctorRegistration.setBackground(new Color(51, 153, 51));
		lblDoctorRegistration.setForeground(new Color(0, 0, 0));
		lblDoctorRegistration.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		lblDoctorRegistration.setBounds(103, 11, 349, 23);
		contentPane.add(lblDoctorRegistration);
		
		taDoctorAddress = new JTextArea();
		taDoctorAddress.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		taDoctorAddress.setBounds(207, 198, 163, 62);
		contentPane.add(taDoctorAddress);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				tfDoctorname.setText("");
				tfDoctorQualification.setText("");
				tfDoctorSpecialisation.setText("");
				tfDoctorExperience.setText("");
				tfDoctorFees.setText("");
				taDoctorAddress.setText("");
				tfDoctorContact.setText("");
				tfDoctorTiming.setText("");
				tfDoctorUsername.setText("");
				pfDoctorPassword.setText("");
				tfDoctorEmail.setText("");
				
			}
		});
		
	}

}
