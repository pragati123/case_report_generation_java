package com.crfg.controller;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;

import com.crfg.model.AppointmentDetail;
import com.crfg.service.AppointmentService;
import com.crfg.service.DoctorService;


import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;



@SuppressWarnings("serial")
public class PatientDashboard extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField txtYyyymmdd;
	private JTextField textField_3;
	protected JTable myReportsTable;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PatientDashboard frame = new PatientDashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PatientDashboard() {
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1440, 800);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel lblNewLabel = new JLabel("Appointment Form");
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNewLabel.setBounds(464, 177, 234, 27);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Patient ID:");
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNewLabel_1.setBounds(386, 220, 113, 36);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Specialization:");
		lblNewLabel_2.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNewLabel_2.setBounds(386, 292, 145, 25);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Doctor Name:");
		lblNewLabel_3.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNewLabel_3.setBounds(386, 344, 123, 27);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Doctor ID:");
		lblNewLabel_4.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNewLabel_4.setBounds(386, 402, 113, 36);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Appointment Date:");
		lblNewLabel_5.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNewLabel_5.setBounds(386, 449, 167, 36);
		getContentPane().add(lblNewLabel_5);
		
		JLabel lblAppointmentTime = new JLabel("Appointment Time:");
		lblAppointmentTime.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblAppointmentTime.setBounds(386, 523, 174, 27);
		getContentPane().add(lblAppointmentTime);
		
		textField = new JTextField();
		textField.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		textField.setBounds(575, 225, 113, 27);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JComboBox<String> cbSpecialization = new JComboBox<String>();
		cbSpecialization.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		cbSpecialization.setBounds(575, 294, 112, 23);
		getContentPane().add(cbSpecialization);
		cbSpecialization.addItem("radiology");
		cbSpecialization.addItem("urology");
		//comboBox.addItem("j");
		
		JComboBox<String> cbDoctorName = new JComboBox<String>();
		cbDoctorName.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		cbDoctorName.setBounds(575, 346, 112, 24);
		getContentPane().add(cbDoctorName);
		//comboBox_1.addItem("r");
		//comboBox_1.addItem("a");
		//comboBox_1.addItem("j");
		
		JButton btnGetDoctorName = new JButton("Get Doctor Name ");
		btnGetDoctorName.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		btnGetDoctorName.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				List<String> doctorName=new ArrayList<String>();
				String doctorSpecialization;
				DoctorService doctorService = new DoctorService();
				
				doctorSpecialization=(String) cbSpecialization.getSelectedItem();
				
				
				
						doctorName=doctorService.getDoctorName(doctorSpecialization);
						
						for (int i = 0; i < doctorName.size(); i++) {
							cbDoctorName.addItem(doctorName.get(i));
							}
						
						
						
						
					} 
				}
			
		);
		btnGetDoctorName.setBounds(721, 294, 181, 23);
		getContentPane().add(btnGetDoctorName);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		textField_3.setBounds(575, 406, 112, 29);
		getContentPane().add(textField_3);		
		textField_3.setColumns(10);

		JButton btnNewButton = new JButton("Get Doctor Id");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
				String doctorName;
				DoctorService doctorService = new DoctorService();
				
				doctorName=(String) cbDoctorName.getSelectedItem();
				String doctorId=Integer.toString(doctorService.getDoctorId(doctorName));
				textField_3.setText(doctorId);	
				        
						
						
			}
		});
		btnNewButton.setBounds(721, 347, 181, 23);
		getContentPane().add(btnNewButton);
		
		
		
		
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		textField_2.setBounds(575, 525, 111, 24);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		txtYyyymmdd = new JTextField();
		txtYyyymmdd.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		txtYyyymmdd.setText("yyyy/mm/dd");
		txtYyyymmdd.setBounds(575, 454, 113, 27);
	getContentPane().add(txtYyyymmdd);
		txtYyyymmdd.setColumns(10);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		btnSubmit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				String s1=textField.getText();
				String s2=(String) cbSpecialization.getSelectedItem();
				String s3=(String) cbDoctorName.getSelectedItem();
				String s4=textField_3.getText();
				String s5=txtYyyymmdd.getText();
				String s6=textField_2.getText();
				try{
					AppointmentDetail e=new AppointmentDetail();
					e.setAppointmentPatientId(Integer.parseInt(s1));
					e.setAppointmentSpecialization(s2);
					e.setAppointmentDoctorName(s3);
					e.setAppointmentDoctorId(Integer.parseInt(s4));
					e.setAppointmentDate(s5);
					e.setAppointmentTime(s6);
					AppointmentService ps=new AppointmentService();
					int x=ps.addAppointmentService(e);
					if(x>0){
						System.out.println("Registered Successfully");
						JOptionPane.showMessageDialog(null,"Your Data saved Successfully!");
					}
					
				}
				 catch (NumberFormatException ex) 
                {
        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
                } 
			}
		});
		btnSubmit.setBounds(531, 611, 133, 50);
		getContentPane().add(btnSubmit);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(255, 255, 153));
		menuBar.setBounds(0, 112, 1382, 44);
		contentPane.add(menuBar);
		
		JMenu mnView = new JMenu("View");
		menuBar.add(mnView);
		
		JMenuItem mntmMyReports = new JMenuItem("Doctor Details");
		mntmMyReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorView doctorview=new DoctorView();
				doctorview.setVisible(true);
				
			}
		});
		mnView.add(mntmMyReports);
		
		JMenuItem mntmMyReports_1 = new JMenuItem("My Reports");
		mntmMyReports_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				MyReports myReports=new MyReports();
				myReports.setVisible(true);
				
				
			}
		});
		mnView.add(mntmMyReports_1);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem mntmUpdateDetails = new JMenuItem("Update Details");
		mntmUpdateDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PatientUpdate patientUpdate=new PatientUpdate();
				patientUpdate.setVisible(true);
			}
		});
		mnEdit.add(mntmUpdateDetails);
		
		JLabel lblPatientDashboard = new JLabel("");
		lblPatientDashboard.setBackground(new Color(153, 204, 51));
		lblPatientDashboard.setFont(new Font("Tahoma", Font.PLAIN, 53));
		lblPatientDashboard.setIcon(new ImageIcon(PatientDashboard.class.getResource("/com/crfg/controller/pics/profile-icon.png")));
		lblPatientDashboard.setBounds(0, 0, 128, 128);
		contentPane.add(lblPatientDashboard);
		
		JLabel lblPatientDashboard_1 = new JLabel("Patient Dashboard");
		lblPatientDashboard_1.setFont(new Font("Tahoma", Font.PLAIN, 56));
		lblPatientDashboard_1.setBounds(442, 0, 551, 112);
		contentPane.add(lblPatientDashboard_1);
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage1 homePage1=new HomePage1();
				homePage1.setVisible(true);
				dispose();
			}
		});
		button.setIcon(new ImageIcon(PatientDashboard.class.getResource("/com/crfg/controller/pics/Apps-session-logout-icon.png")));
		button.setBounds(1201, 11, 95, 90);
		contentPane.add(button);
	
	}
}