package com.crfg.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.crfg.service.PatientService;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

@SuppressWarnings("serial")
public class MyReports extends JFrame {

	private JPanel contentPane;
	private JTextField tfMyReportsId;
	private JTable myReportsTable;
	private JLabel lblMyReports;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyReports frame = new MyReports();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyReports() {
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 865, 475);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 102, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEnterId = new JLabel("Enter Id");
		lblEnterId.setForeground(new Color(255, 204, 0));
		lblEnterId.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblEnterId.setBounds(253, 12, 86, 45);
		contentPane.add(lblEnterId);
		
		tfMyReportsId = new JTextField();
		tfMyReportsId.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfMyReportsId.setBounds(349, 18, 116, 32);
		contentPane.add(tfMyReportsId);
		tfMyReportsId.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(30, 81, 809, 344);
		contentPane.add(scrollPane);
		
		myReportsTable = new JTable();
		scrollPane.setViewportView(myReportsTable);
		
		JButton btnViewMyReports = new JButton("View Reports");
		btnViewMyReports.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnViewMyReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
           
				PatientService patientService=new PatientService();
				int patientId=Integer.parseInt(tfMyReportsId.getText());
				patientService.viewGeneratedReport(myReportsTable, patientId);
			}
		});
		btnViewMyReports.setBounds(503, 23, 146, 23);
		contentPane.add(btnViewMyReports);
		
		lblMyReports = new JLabel("My Reports");
		lblMyReports.setForeground(new Color(255, 204, 0));
		lblMyReports.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblMyReports.setBounds(30, 0, 263, 81);
		contentPane.add(lblMyReports);
	}
}