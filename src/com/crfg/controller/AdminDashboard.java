package com.crfg.controller;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import com.crfg.service.AdminService;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

@SuppressWarnings("serial")


public class AdminDashboard extends JFrame{
	
AdminService adminService =new AdminService();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminDashboard frame = new AdminDashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminDashboard() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1378, 730);
		JPanel contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 128, 1352, 613);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 204, 102));
		tabbedPane.addTab("Doctor", null, panel, null);
		panel.setLayout(null);
		
		JTextField tfDoctorIdDelete = new JTextField();
		tfDoctorIdDelete.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorIdDelete.setBounds(1175, 214, 128, 30);
		panel.add(tfDoctorIdDelete);
		tfDoctorIdDelete.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 49, 1142, 439);
		panel.add(scrollPane);
		
		JTable doctorTable = new JTable();
		scrollPane.setViewportView(doctorTable);
		
		doctorTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int selectedRowIndex = doctorTable.getSelectedRow();
				tfDoctorIdDelete.setText(doctorTable.getValueAt(selectedRowIndex, 0).toString());
				
			}
		});
		
		JButton btnDoctorView = new JButton("View ");
		btnDoctorView.setBackground(new Color(0, 204, 204));
		btnDoctorView.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDoctorView.setBounds(0, 488, 1142, 45);
		panel.add(btnDoctorView);
		btnDoctorView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				adminService.displayDoctor(doctorTable);
			}
		});
		
		JButton btnDoctorDelete = new JButton("Delete Doctor");
		btnDoctorDelete.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDoctorDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int doctorId =Integer.parseInt(tfDoctorIdDelete.getText());
				adminService.deleteDoctorDetail(doctorId);
						
			}
		});
		btnDoctorDelete.setBounds(1177, 263, 137, 23);
		panel.add(btnDoctorDelete);
		
		JLabel lblDoctorIdDelete = new JLabel("Id to be deleted");
		lblDoctorIdDelete.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorIdDelete.setBounds(1175, 165, 128, 38);
		panel.add(lblDoctorIdDelete);
		
		JLabel lblDoctorDetail = new JLabel("Doctor Details");
		lblDoctorDetail.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblDoctorDetail.setBounds(481, 0, 735, 61);
		panel.add(lblDoctorDetail);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 204, 102));
		tabbedPane.addTab("Patient", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 50, 1307, 433);
		panel_1.add(scrollPane_1);
		
		JTable patientTable = new JTable();
		scrollPane_1.setViewportView(patientTable);
		
		JButton btnPatientView = new JButton("View");
		btnPatientView.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnPatientView.setBounds(10, 494, 1307, 37);
		panel_1.add(btnPatientView);
		btnPatientView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				adminService.displayPatient(patientTable);
			}
		});
		
		JLabel lblPatientDetail = new JLabel("Patient Detail");
		lblPatientDetail.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPatientDetail.setBounds(371, 0, 215, 60);
		panel_1.add(lblPatientDetail);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 204, 102));
		tabbedPane.addTab("Feedback", null, panel_2, null);
		panel_2.setLayout(null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 49, 1297, 421);
		panel_2.add(scrollPane_2);
		
		JTable feedbackTable = new JTable();
		scrollPane_2.setViewportView(feedbackTable);
		
		JButton btnFeedbackView = new JButton("View");
		btnFeedbackView.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnFeedbackView.setBounds(10, 481, 1297, 46);
		panel_2.add(btnFeedbackView);
		btnFeedbackView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				adminService.displayFeedback(feedbackTable);
			}
		});
				
		JLabel lblFeedbacks = new JLabel("Feedbacks");
		lblFeedbacks.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblFeedbacks.setBounds(433, 0, 222, 54);
		panel_2.add(lblFeedbacks);
		
		
		
		JLabel lblAdminDashboard = new JLabel("Admin Dashboard");
		lblAdminDashboard.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblAdminDashboard.setBounds(394, 51, 337, 59);
		contentPane.add(lblAdminDashboard);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(AdminDashboard.class.getResource("/com/crfg/controller/pics/profile-icon.png")));
		label.setBounds(0, 0, 128, 128);
		contentPane.add(label);
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage1 homePage1=new HomePage1();
				homePage1.setVisible(true);
				dispose();
			}
		});
		button.setIcon(new ImageIcon(AdminDashboard.class.getResource("/com/crfg/controller/pics/Apps-session-logout-icon.png")));
		button.setBounds(1181, 27, 100, 90);
		contentPane.add(button);
	}

}
