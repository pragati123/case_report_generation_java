package com.crfg.controller;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class HomePage {

	private JFrame frame;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePage window = new HomePage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public HomePage() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(204, 204, 51));
		frame.setBackground(Color.WHITE);
		frame.setBounds(0, 0,1440, 750);
 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
	    JLabel admin_icon = new JLabel("");
		admin_icon.setIcon(new ImageIcon(HomePage.class.getResource("/com/crfg/controller/pics/admin_icon.png")));
		admin_icon.setBounds(156, 202, 204, 250);
		frame.getContentPane().add(admin_icon);
		
		JLabel doctor_icon = new JLabel("");
		doctor_icon.setIcon(new ImageIcon(HomePage.class.getResource("/com/crfg/controller/pics/doctor_icon.png")));
		doctor_icon.setBounds(518, 202, 255, 250);
		frame.getContentPane().add(doctor_icon);
		
		JButton btnAdminlogin = new JButton("Admin Login");
		btnAdminlogin.setForeground(new Color(255, 0, 0));
		btnAdminlogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnAdminlogin.setBackground(new Color(255, 255, 255));
		btnAdminlogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AdminLogin adminlogin=new AdminLogin();
				adminlogin.setVisible(true);
				frame.dispose();
			}
		});
		
		JLabel patient_icon = new JLabel("");
		patient_icon.setIcon(new ImageIcon(HomePage.class.getResource("/com/crfg/controller/pics/patient_icon.jpg")));
		patient_icon.setBounds(1018, 202, 216, 250);
		frame.getContentPane().add(patient_icon);
		btnAdminlogin.setBounds(194, 484, 129, 34);
		frame.getContentPane().add(btnAdminlogin);
		
		JButton btnDoctorlogin = new JButton("Doctor Login");
		btnDoctorlogin.setBackground(new Color(255, 255, 255));
		btnDoctorlogin.setForeground(new Color(255, 0, 0));
		btnDoctorlogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDoctorlogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DoctorLogin doctorlogin=new DoctorLogin();
				doctorlogin.setVisible(true);
				
			}
		});
		btnDoctorlogin.setBounds(582, 484, 139, 34);
		frame.getContentPane().add(btnDoctorlogin);
		
		JButton btnPatientLogin = new JButton("Patient Login");
		btnPatientLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PatientLogin patientlogin=new PatientLogin();
				patientlogin.setVisible(true);
				frame.dispose();
			}
		});
		btnPatientLogin.setBackground(new Color(255, 255, 255));
		btnPatientLogin.setForeground(new Color(255, 0, 0));
		btnPatientLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnPatientLogin.setBounds(1072, 484, 139, 34);
		frame.getContentPane().add(btnPatientLogin);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(HomePage.class.getResource("/com/crfg/controller/pics/LoginBackground.jpg")));
		label.setBounds(0, 160, 1200, 480);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(HomePage.class.getResource("/com/crfg/controller/pics/LoginBackground.jpg")));
		label_1.setBounds(0, 438, 1200, 550);
		frame.getContentPane().add(label_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(HomePage.class.getResource("/com/crfg/controller/pics/LoginBackground.jpg")));
		lblNewLabel.setBounds(154, 161, 1200, 550);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblCaseReportForm = new JLabel("CASE REPORT  FORM GENERATION");
		lblCaseReportForm.setForeground(new Color(255, 0, 0));
		lblCaseReportForm.setBackground(new Color(51, 204, 204));
		lblCaseReportForm.setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
		lblCaseReportForm.setBounds(294, 41, 976, 93);
		frame.getContentPane().add(lblCaseReportForm);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(493, 536, 46, 14);
		frame.getContentPane().add(label_2);
	}
}