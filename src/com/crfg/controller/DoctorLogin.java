package com.crfg.controller;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.crfg.service.DoctorService;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")

public class DoctorLogin extends JFrame{
	private JPanel contentPane;
	private JTextField tfDoctorUsername;
	private JPasswordField pfDoctorPassword;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DoctorLogin frame = new DoctorLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DoctorLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 392, 419);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDoctorLogin = new JLabel("Doctor Login");
		lblDoctorLogin.setForeground(Color.RED);
		lblDoctorLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		lblDoctorLogin.setBackground(Color.WHITE);
		lblDoctorLogin.setBounds(115, 53, 140, 40);
		contentPane.add(lblDoctorLogin);
		
		JLabel lblDoctorUserName = new JLabel("User name");
		lblDoctorUserName.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		lblDoctorUserName.setBounds(45, 115, 93, 25);
		contentPane.add(lblDoctorUserName);
		
		JLabel lblDoctorPassword = new JLabel("Password");
		lblDoctorPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		lblDoctorPassword.setBounds(45, 163, 74, 25);
		contentPane.add(lblDoctorPassword);
		
		tfDoctorUsername = new JTextField();
		tfDoctorUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		tfDoctorUsername.setColumns(10);
		tfDoctorUsername.setBounds(171, 117, 115, 20);
		contentPane.add(tfDoctorUsername);
		
		pfDoctorPassword = new JPasswordField();
		pfDoctorPassword.setBounds(171, 167, 115, 20);
		contentPane.add(pfDoctorPassword);
		
		JButton btnDoctorLogin = new JButton("Login");
		btnDoctorLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnDoctorLogin.setBounds(120, 222, 93, 35);
		contentPane.add(btnDoctorLogin);
		btnDoctorLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username=tfDoctorUsername.getText();
				char[] password = pfDoctorPassword.getPassword();
				String passwordString = new String(password);
				DoctorService doctorService = new DoctorService();
				
				boolean validate=doctorService.vaidateDoctor(username,passwordString);
				if(validate){
					JOptionPane.showMessageDialog(btnDoctorLogin,"You have successfully logged in");
					DoctorDashboard doctorDashboard = new DoctorDashboard();
					doctorDashboard.setVisible(true);
					dispose();
					
				}
				else{
					JOptionPane.showMessageDialog(btnDoctorLogin,"You are not valid user!");
					
				}

			}
		});
		
		JLabel lblDoctorNotMember = new JLabel("Not a memeber yet??");
		lblDoctorNotMember.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		lblDoctorNotMember.setBounds(23, 305, 150, 25);
		contentPane.add(lblDoctorNotMember);
		
		JButton btnDoctorRegister = new JButton("Register Here");
		btnDoctorRegister.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnDoctorRegister.setBounds(183, 297, 146, 40);
		contentPane.add(btnDoctorRegister);
		btnDoctorRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DoctorRegistration doctorRegistration=new DoctorRegistration();
				doctorRegistration.setVisible(true);
				dispose();
			}
		});
		
		JLabel label_4 = new JLabel("");
		label_4.setBounds(339, 0, 390, 401);
		contentPane.add(label_4);
	}

}
