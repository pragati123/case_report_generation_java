package com.crfg.controller;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.crfg.model.ReportDetail;
import com.crfg.service.ReportService;

@SuppressWarnings("serial")
public class ReportGeneration extends JFrame {

	private JPanel contentPane;
	protected ReportDetail reportDetail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReportGeneration frame = new ReportGeneration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReportGeneration() {
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 1, 600, 763);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblReportGenerationForm = new JLabel("Report Generation Form");
		lblReportGenerationForm.setForeground(Color.BLUE);
		lblReportGenerationForm.setFont(new Font("Footlight MT Light", Font.BOLD, 20));
		lblReportGenerationForm.setBounds(199, 11, 256, 30);
		getContentPane().add(lblReportGenerationForm);
		
		JLabel lblDoctorId = new JLabel("Doctor Id");
		lblDoctorId.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblDoctorId.setBounds(35, 55, 83, 14);
		getContentPane().add(lblDoctorId);
		
		JLabel lblPatientId = new JLabel("Patient Id");
		lblPatientId.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientId.setBounds(35, 84, 114, 14);
		getContentPane().add(lblPatientId);
		
		JLabel lblPatientName = new JLabel("Patient Name");
		lblPatientName.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientName.setBounds(35, 114, 98, 14);
		getContentPane().add(lblPatientName);
		
		JLabel lblReportGenerationDate = new JLabel("Report Generation Date");
		lblReportGenerationDate.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblReportGenerationDate.setBounds(35, 139, 153, 14);
		getContentPane().add(lblReportGenerationDate);
		
		JLabel lblReportType = new JLabel("Report Type");
		lblReportType.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblReportType.setBounds(35, 183, 98, 14);
		getContentPane().add(lblReportType);
		
		JTextField tfPatientId = new JTextField();
		tfPatientId.setBounds(350, 81, 169, 20);
		getContentPane().add(tfPatientId);
		tfPatientId.setColumns(10);
		
		JTextField tfPatientName = new JTextField();
		tfPatientName.setBounds(350, 111, 169, 20);
		getContentPane().add(tfPatientName);
		tfPatientName.setColumns(10);
		
		JTextField tfReportFees = new JTextField();
		tfReportFees.setBounds(350, 218, 169, 20);
		getContentPane().add(tfReportFees);
		tfReportFees.setColumns(10);
		
		JTextField tfDoctorId = new JTextField();
		tfDoctorId.setBounds(350, 52, 169, 20);
		getContentPane().add(tfDoctorId);
		tfDoctorId.setColumns(10);
		
		JLabel lblReportFees = new JLabel("Report Fees");
		lblReportFees.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblReportFees.setBounds(35, 221, 114, 14);
		getContentPane().add(lblReportFees);
		
		JLabel lblPatientHeight = new JLabel("Patient height");
		lblPatientHeight.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientHeight.setBounds(35, 246, 114, 14);
		getContentPane().add(lblPatientHeight);
		
		JTextField tfPatientHeight = new JTextField();
		tfPatientHeight.setBounds(350, 243, 169, 20);
		getContentPane().add(tfPatientHeight);
		tfPatientHeight.setColumns(10);
		
		JLabel lblPatientWeight = new JLabel("Patient Weight");
		lblPatientWeight.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientWeight.setBounds(35, 283, 114, 14);
		getContentPane().add(lblPatientWeight);
		
		JLabel lblPatientHaemoglobin = new JLabel("Patient Haemoglobin");
		lblPatientHaemoglobin.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientHaemoglobin.setBounds(35, 365, 153, 14);
		getContentPane().add(lblPatientHaemoglobin);
		
		JLabel lblPatientWBC = new JLabel("Patient WBC");
		lblPatientWBC.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientWBC.setBounds(35, 390, 114, 14);
		getContentPane().add(lblPatientWBC);
		
		JLabel lblPatientBloodGroup = new JLabel("Patient Blood Group");
		lblPatientBloodGroup.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientBloodGroup.setBounds(35, 326, 137, 14);
		getContentPane().add(lblPatientBloodGroup);
		
		JLabel lblPatientRBC = new JLabel("Patient RBC");
		lblPatientRBC.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientRBC.setBounds(35, 415, 114, 14);
		getContentPane().add(lblPatientRBC);
		
		JLabel lblPatientPlatelet = new JLabel("Patient Platelet");
		lblPatientPlatelet.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientPlatelet.setBounds(35, 449, 137, 14);
		getContentPane().add(lblPatientPlatelet);
		
		JLabel lblPatientTemperature = new JLabel("Patient Body Temperature (in Fahrenheit)");
		lblPatientTemperature.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientTemperature.setBounds(35, 474, 263, 14);
		getContentPane().add(lblPatientTemperature);
		
		JLabel lblPatientSystolic = new JLabel("Patient Systolic");
		lblPatientSystolic.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientSystolic.setBounds(35, 541, 114, 14);
		getContentPane().add(lblPatientSystolic);
		
		JLabel lblPatientVision = new JLabel("Patient Vision");
		lblPatientVision.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientVision.setBounds(35, 516, 114, 14);
		getContentPane().add(lblPatientVision);
		
		JLabel lblPatientDiastolic = new JLabel("Patient Diastolic");
		lblPatientDiastolic.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientDiastolic.setBounds(35, 575, 114, 14);
		getContentPane().add(lblPatientDiastolic);
		
		JLabel lblPatientRespiratoryRate = new JLabel("Patient Respiratory Rate");
		lblPatientRespiratoryRate.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientRespiratoryRate.setBounds(35, 600, 161, 14);
		getContentPane().add(lblPatientRespiratoryRate);
		
		JLabel lblPatientResult = new JLabel("Patient Result");
		lblPatientResult.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientResult.setBounds(35, 625, 98, 14);
		getContentPane().add(lblPatientResult);
		
		JLabel label = new JLabel("");
		label.setBounds(199, 575, 46, 14);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(274, 516, 46, 14);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(199, 390, 46, 14);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(336, 371, 46, 14);
		getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("");
		label_4.setBounds(359, 415, 46, 14);
		getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("");
		label_5.setBounds(324, 472, 46, 14);
		getContentPane().add(label_5);
		
		JTextField tfPatientWeight = new JTextField();
		tfPatientWeight.setColumns(10);
		tfPatientWeight.setBounds(350, 274, 169, 20);
		getContentPane().add(tfPatientWeight);
		
		JTextField tfPatientHaemoglobin = new JTextField();
		tfPatientHaemoglobin.setColumns(10);
		tfPatientHaemoglobin.setBounds(350, 362, 169, 20);
		getContentPane().add(tfPatientHaemoglobin);
		
		JTextField tfPatientWBC = new JTextField();
		tfPatientWBC.setColumns(10);
		tfPatientWBC.setBounds(350, 387, 169, 20);
		getContentPane().add(tfPatientWBC);
		
		JTextField tfPatientRBC = new JTextField();
		tfPatientRBC.setColumns(10);
		tfPatientRBC.setBounds(350, 415, 169, 20);
		getContentPane().add(tfPatientRBC);
		
		JTextField tfPatientPlatelet = new JTextField();
		tfPatientPlatelet.setColumns(10);
		tfPatientPlatelet.setBounds(350, 446, 169, 20);
		getContentPane().add(tfPatientPlatelet);
		
		JTextField tfPatientBodyTemperature = new JTextField();
		tfPatientBodyTemperature.setColumns(10);
		tfPatientBodyTemperature.setBounds(350, 472, 169, 20);
		getContentPane().add(tfPatientBodyTemperature);
		
		JTextField tfPatientVision = new JTextField();
		tfPatientVision.setColumns(10);
		tfPatientVision.setBounds(350, 510, 169, 20);
		getContentPane().add(tfPatientVision);
		
		JTextField tfPatientSystolic = new JTextField();
		tfPatientSystolic.setColumns(10);
		tfPatientSystolic.setBounds(350, 538, 169, 20);
		getContentPane().add(tfPatientSystolic);
		
		JTextField tfPatientDiastolic = new JTextField();
		tfPatientDiastolic.setColumns(10);
		tfPatientDiastolic.setBounds(350, 569, 169, 20);
		getContentPane().add(tfPatientDiastolic);
		
		JTextField tfPatientRespiratoryRate = new JTextField();
		tfPatientRespiratoryRate.setColumns(10);
		tfPatientRespiratoryRate.setBounds(350, 597, 169, 20);
		getContentPane().add(tfPatientRespiratoryRate);
		
		JTextField tfPatientResult = new JTextField();
		tfPatientResult.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		tfPatientResult.setColumns(10);
		tfPatientResult.setBounds(350, 625, 169, 20);
		getContentPane().add(tfPatientResult);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		btnSubmit.setBounds(199, 672, 89, 23);
		getContentPane().add(btnSubmit);
		

		JTextField txtYyyymmdd = new JTextField();
		txtYyyymmdd.setText("yyyy/mm/dd");
		txtYyyymmdd.setBounds(350, 136, 169, 20);
		getContentPane().add(txtYyyymmdd);
		txtYyyymmdd.setColumns(10);
		
		JComboBox<String> cbBloodGroup = new JComboBox<String>();
		cbBloodGroup.setBounds(350, 318, 169, 30);
		getContentPane().add(cbBloodGroup);
	
		cbBloodGroup.addItem("Select Blood Group");
		cbBloodGroup.addItem("A+");
		cbBloodGroup.addItem("B+");
		cbBloodGroup.addItem("AB+");
		cbBloodGroup.addItem("O+");
		cbBloodGroup.addItem("O-");
		cbBloodGroup.addItem("A-");
		cbBloodGroup.addItem("B-");
		cbBloodGroup.addItem("AB-");
		
		JComboBox<String> cbReportType = new JComboBox<String>();
		cbReportType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
			}
		});
		cbReportType.setToolTipText("Select Report Type");
		cbReportType.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		cbReportType.setBounds(350, 180, 169, 20);
		getContentPane().add(cbReportType);
		
		cbReportType.addItem("Select Report Type");
		cbReportType.addItem("Typhoid");
		cbReportType.addItem("Maleria");
		cbReportType.addItem("Dengue");
		cbReportType.addItem("Jaundice");
		cbReportType.addItem("Blood Cancer");
		cbReportType.addItem("tuberculous");
		cbReportType.addItem("Eye sight");
		cbReportType.addItem("Cardiomypathy");
		cbReportType.addItem("Systolic Heart Failure");
		cbReportType.addItem("Diastolic Heart Failure");
		cbReportType.addItem("High Blood Pressure");
		cbReportType.addItem("Low Blood Pressure");
		cbReportType.addItem("BMI");
		cbReportType.addItem("Waist Circumference");
		
				
		btnSubmit.addActionListener(new ActionListener() {
			
			private int doctorId;
			private int patientId;
			private String patientName;
			private String reportDate;
			private String reportType;
			private float reportFees;
			private float patientHeight;
			private float patientWeight;
			private String patientBloodGroup;
			private float patientHaemoglobin;
			private float patientWBC;
			private float patientRBC;
			private float patientPlatelet;
			private float patientTemperature;
			private String patientVision;
			private float patientSystolic;
			private int patientRespiratoryRate;
			private float patientDiastolic;
			
			public void actionPerformed(ActionEvent arg0) {
				
				ReportDetail reportDetail=new ReportDetail();
				try{
				
				doctorId=Integer.parseInt(tfDoctorId.getText());
				patientId=Integer.parseInt(tfPatientId.getText());
				patientName=tfPatientName.getText();
				reportDate=txtYyyymmdd.getText();
				reportType=(String)cbReportType.getSelectedItem();
				reportFees=Float.parseFloat(tfReportFees.getText());
				patientHeight=Float.parseFloat(tfPatientHeight.getText());
				patientWeight=Float.parseFloat(tfPatientWeight.getText());
				patientBloodGroup=(String)cbBloodGroup.getSelectedItem();
				patientHaemoglobin=Float.parseFloat(tfPatientHaemoglobin.getText());
				patientWBC=Float.parseFloat(tfPatientWBC.getText());
				patientRBC=Float.parseFloat(tfPatientRBC.getText());
				patientPlatelet=Float.parseFloat(tfPatientPlatelet.getText());
				patientTemperature=Float.parseFloat(tfPatientBodyTemperature.getText());
				patientVision=tfPatientVision.getText();
				patientSystolic=Float.parseFloat(tfPatientSystolic.getText());
				patientDiastolic=Float.parseFloat(tfPatientDiastolic.getText());
				patientRespiratoryRate=Integer.parseInt(tfPatientRespiratoryRate.getText());
				
				reportDetail.setReportDoctorId(doctorId);
				reportDetail.setReportPatientId(patientId);
				reportDetail.setPatientName(patientName);
				reportDetail.setReportDate(reportDate);
				reportDetail.setReportType(reportType);
				reportDetail.setReportFees(reportFees);
				reportDetail.setReportHeight(patientHeight);
				reportDetail.setReportWeight(patientWeight);
				reportDetail.setReportBloodGroup(patientBloodGroup);
				reportDetail.setReportHaemoglobin(patientHaemoglobin);
				reportDetail.setReportWBC(patientWBC);
				reportDetail.setReportRBC(patientRBC);
				reportDetail.setReportPlatelet(patientPlatelet);
				reportDetail.setReportTemperatureF(patientTemperature);
				reportDetail.setReportVision(patientVision);
				reportDetail.setReportBloodSystolic(patientSystolic);
				reportDetail.setReportBloodDiastolic(patientDiastolic);
				reportDetail.setReportRespiratoryRate(patientRespiratoryRate);
				String reportResult=tfPatientResult.getText();
				reportDetail.setReportResult(reportResult);

				ReportService reportService=new ReportService();
				int x = 0;
				try {
					x = reportService.addReport(reportDetail);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			if(x>0){
				JOptionPane.showMessageDialog(null, "Report Generated. Thank You");
				ViewReport viewReport = new ViewReport(reportDetail);
				viewReport.setVisible(true);
			}
				}
			 catch (NumberFormatException ex) 
            {
    	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
            } 
							
			
			}
		});
		
		JButton btnClear = new JButton("Clear");
		btnClear.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		btnClear.setBounds(387, 672, 89, 23);
		getContentPane().add(btnClear);
		
		JButton btnDisplayResult = new JButton("Display Result");
		btnDisplayResult.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		btnDisplayResult.setBounds(529, 621, 137, 23);
		getContentPane().add(btnDisplayResult);
		btnDisplayResult.addActionListener(new ActionListener() {
			private float patientHaemoglobin;
			private float patientWBC;
			private float patientRBC;
			private float patientPlatelet;
			private float patientTemperature;
			private String patientVision;
			private float patientSystolic;
			private float patientDiastolic;
			private int patientRespiratoryRate;

			public void actionPerformed(ActionEvent arg0) {
				patientHaemoglobin=Float.parseFloat(tfPatientHaemoglobin.getText());
				patientWBC=Float.parseFloat(tfPatientWBC.getText());
				patientRBC=Float.parseFloat(tfPatientRBC.getText());
				patientPlatelet=Float.parseFloat(tfPatientPlatelet.getText());
				patientTemperature=Float.parseFloat(tfPatientBodyTemperature.getText());
				patientVision=tfPatientVision.getText();
				patientSystolic=Float.parseFloat(tfPatientSystolic.getText());
				patientDiastolic=Float.parseFloat(tfPatientDiastolic.getText());
				patientRespiratoryRate=Integer.parseInt(tfPatientRespiratoryRate.getText());
				if((patientHaemoglobin >= 13 && patientHaemoglobin <=16) && (patientWBC >=4500 && patientWBC <=11000) && (patientRBC >= 4.2 && patientRBC <=6.1) && (patientPlatelet >= 150000 && patientPlatelet<= 450000) && (patientTemperature >= 97.7 && patientTemperature<= 99.5) && (patientVision.equals("6/6")) && (patientSystolic >= 110  && patientSystolic <= 120) && (patientDiastolic >= 70 && patientDiastolic <=80) && (patientRespiratoryRate >= 12 && patientRespiratoryRate <= 18))
				{
					tfPatientResult.setText("Negative");
					
				}
				else{
					tfPatientResult.setText("Positive");

				}
			}
		});
		
		
		
		
		btnClear.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent arg0) {
				
				tfDoctorId.setText("");
				tfPatientId.setText("");
				tfPatientName.setText("");
				cbReportType.setSelectedItem(null);
				tfReportFees.setText("");
				tfPatientHeight.setText("");
				tfPatientWeight.setText("");
				cbBloodGroup.setSelectedItem(null);
				tfPatientHaemoglobin.setText("");
				tfPatientWBC.setText("");
				tfPatientRBC.setText("");
				tfPatientPlatelet.setText("");
				tfPatientBodyTemperature.setText("");
				tfPatientVision.setText("");
				tfPatientSystolic.setText("");
				tfPatientDiastolic.setText("");
				tfPatientRespiratoryRate.setText("");
				tfPatientResult.setText("");
				txtYyyymmdd.setText("");		
			}
		});
	}
}