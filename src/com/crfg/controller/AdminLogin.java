package com.crfg.controller;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.crfg.service.AdminService;

@SuppressWarnings("serial")

public class AdminLogin extends JFrame{
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminLogin frame = new AdminLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminLogin() {
				
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 588, 352);
		JPanel contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAdminLogin = new JLabel("Admin Login");
		lblAdminLogin.setForeground(Color.RED);
		lblAdminLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		lblAdminLogin.setBackground(Color.WHITE);
		lblAdminLogin.setBounds(234, 47, 173, 40);
		contentPane.add(lblAdminLogin);
		
		JLabel lblAdminUsername = new JLabel("User name");
		lblAdminUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblAdminUsername.setBounds(115, 98, 93, 25);
		contentPane.add(lblAdminUsername);
		
		JLabel lblAdminPassword = new JLabel("Password");
		lblAdminPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblAdminPassword.setBounds(115, 153, 74, 25);
		contentPane.add(lblAdminPassword);
		
		JTextField tfUsername = new JTextField();
		tfUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfUsername.setColumns(10);
		tfUsername.setBounds(408, 98, 115, 20);
		contentPane.add(tfUsername);
		
		JPasswordField pfPassword = new JPasswordField();
		pfPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		pfPassword.setBounds(408, 155, 115, 20);
		contentPane.add(pfPassword);
		
		JButton btnAdminLogin = new JButton("Login");
		btnAdminLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnAdminLogin.setBounds(234, 230, 93, 35);
		contentPane.add(btnAdminLogin);
		
		btnAdminLogin.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String username=tfUsername.getText();
				char[] password = pfPassword.getPassword();
				String passwordString = new String(password);
				AdminService adminService = new AdminService();
				boolean validate=adminService.validateAdmin(username,passwordString);
				if(validate){
					JOptionPane.showMessageDialog(btnAdminLogin,"You have successfully logged in");
					AdminDashboard adminDashboard = new AdminDashboard();
					adminDashboard.setVisible(true);
					dispose();
					
				}
				else{
					JOptionPane.showMessageDialog(btnAdminLogin,"You are not valid user!");
					
				}
				
									
			}
		});
		
	}

}
