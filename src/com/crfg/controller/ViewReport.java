package com.crfg.controller;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.crfg.model.ReportDetail;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;

@SuppressWarnings("serial")
public class ViewReport extends JFrame {

	protected static final String ReportGeneration = null;
	private JPanel contentPane;
	private JTextField tfReportViewPatientName;
	private JTextField tfReportViewDate;
	private JTextField tfReportViewFees;
	private JTextField tfReportViewHeight;
	private JTextField tfReportViewWeight;
	private JTextField tfReportViewHaemoglobin;
	private JTextField tfReportViewWBC;
	private JTextField tfReportViewRBC;
	private JTextField tfReportViewPlatelet;
	private JTextField tfReportViewBodyTemperatue;
	private JTextField tfReportViewVision;
	private JTextField tfReportViewSystolic;
	private JTextField tfReportViewDiastolic;
	private JTextField tfReportViewRespiratoryRate;
	private JTextField tfReportViewResult;
	private JTextField tfReportViewDoctorId;
	private JTextField tfReportViewPatientId;
	private JTextField tfReportViewReportType;
	private JTextField tfReportViewBloodGroup;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewReport frame = new ViewReport(new ReportDetail());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewReport(ReportDetail reportDetail)
	{
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, -70, 573, 726);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPatientReport = new JLabel("PATIENT REPORT");
		lblPatientReport.setFont(new Font("Footlight MT Light", Font.BOLD, 19));
		lblPatientReport.setBounds(168, 11, 179, 28);
		contentPane.add(lblPatientReport);
		
		JLabel lblDoctorId = new JLabel("Doctor Id");
		lblDoctorId.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblDoctorId.setBounds(10, 48, 83, 14);
		
		contentPane.add(lblDoctorId);
		
		JLabel lblPatientId = new JLabel("Patient Id");
		lblPatientId.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientId.setBounds(10, 79, 114, 14);
		contentPane.add(lblPatientId);
		
		JLabel lblPatientName = new JLabel("Patient Name");
		lblPatientName.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPatientName.setBounds(10, 113, 98, 14);
		contentPane.add(lblPatientName);
		
		JLabel lblRespiratoryRate = new JLabel("Report Generation Date");
		lblRespiratoryRate.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblRespiratoryRate.setBounds(10, 154, 153, 14);
		contentPane.add(lblRespiratoryRate);
		
		JLabel lblReportType = new JLabel("Report Type");
		lblReportType.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblReportType.setBounds(10, 191, 98, 14);
		contentPane.add(lblReportType);
		
		JLabel lblHeight = new JLabel("Patient height");
		lblHeight.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblHeight.setBounds(10, 258, 114, 14);
		contentPane.add(lblHeight);
		
		JLabel lblReportFees = new JLabel("Report Fees");
		lblReportFees.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblReportFees.setBounds(10, 226, 114, 14);
		contentPane.add(lblReportFees);
		
		JLabel lblWeight = new JLabel("Patient Weight");
		lblWeight.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblWeight.setBounds(10, 295, 114, 14);
		contentPane.add(lblWeight);
		
		JLabel lblBloodGroup = new JLabel("Patient Blood Group");
		lblBloodGroup.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblBloodGroup.setBounds(10, 326, 137, 14);
		contentPane.add(lblBloodGroup);
		
		JLabel lblHaemoglobin = new JLabel("Patient Haemoglobin");
		lblHaemoglobin.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblHaemoglobin.setBounds(10, 351, 153, 14);
		contentPane.add(lblHaemoglobin);
		
		JLabel lblWBC = new JLabel("Patient WBC");
		lblWBC.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblWBC.setBounds(10, 390, 114, 14);
		contentPane.add(lblWBC);
		
		JLabel lblRBC = new JLabel("Patient RBC");
		lblRBC.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblRBC.setBounds(10, 421, 114, 14);
		contentPane.add(lblRBC);
		
		JLabel lblPlatelet = new JLabel("Patient Platelet");
		lblPlatelet.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblPlatelet.setBounds(10, 455, 137, 14);
		contentPane.add(lblPlatelet);
		
		JLabel lblBodyTempearture = new JLabel("Patient Body Temperature (in Fahrenheit)");
		lblBodyTempearture.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblBodyTempearture.setBounds(10, 480, 263, 14);
		contentPane.add(lblBodyTempearture);
		
		JLabel lblVision = new JLabel("Patient Vision");
		lblVision.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblVision.setBounds(10, 522, 114, 14);
		contentPane.add(lblVision);
		
		JLabel lblSystolic = new JLabel("Patient Systolic");
		lblSystolic.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblSystolic.setBounds(10, 547, 114, 14);
		contentPane.add(lblSystolic);
		
		JLabel lblDiastolic = new JLabel("Patient Diastolic");
		lblDiastolic.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblDiastolic.setBounds(10, 572, 114, 14);
		contentPane.add(lblDiastolic);
		
		JLabel lblRespiartoryRate = new JLabel("Patient Respiratory Rate");
		lblRespiartoryRate.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblRespiartoryRate.setBounds(10, 609, 161, 14);
		contentPane.add(lblRespiartoryRate);
		
		JLabel lblResult = new JLabel("Patient Result");
		lblResult.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		lblResult.setBounds(10, 634, 98, 14);
		contentPane.add(lblResult);
		
		tfReportViewPatientName = new JTextField();
		tfReportViewPatientName.setColumns(10);
		tfReportViewPatientName.setBounds(283, 110, 169, 20);
		tfReportViewPatientName.setEditable(false);
		tfReportViewPatientName.setText(reportDetail.getPatientName());
		contentPane.add(tfReportViewPatientName);
		
		tfReportViewDate = new JTextField();
		tfReportViewDate.setText("yyyy/mm/dd");
		tfReportViewDate.setColumns(10);
		tfReportViewDate.setBounds(283, 151, 169, 20);
		tfReportViewDate.setEditable(false);
		tfReportViewDate.setText(reportDetail.getReportDate());
		contentPane.add(tfReportViewDate);
		
		tfReportViewFees = new JTextField();
		tfReportViewFees.setColumns(10);
		tfReportViewFees.setBounds(283, 223, 169, 20);
		tfReportViewFees.setEditable(false);
		tfReportViewFees.setText(String.valueOf(reportDetail.getReportFees()));
		contentPane.add(tfReportViewFees);
		
		tfReportViewHeight = new JTextField();
		tfReportViewHeight.setColumns(10);
		tfReportViewHeight.setBounds(283, 255, 169, 20);
		tfReportViewHeight.setEditable(false);
		tfReportViewHeight.setText(String.valueOf(reportDetail.getReportHeight()));
		contentPane.add(tfReportViewHeight);
		
		tfReportViewWeight = new JTextField();
		tfReportViewWeight.setColumns(10);
		tfReportViewWeight.setBounds(283, 292, 169, 20);
		tfReportViewWeight.setEditable(false);
		tfReportViewWeight.setText(String.valueOf(reportDetail.getReportWeight()));
		contentPane.add(tfReportViewWeight);
		
		tfReportViewHaemoglobin = new JTextField();
		tfReportViewHaemoglobin.setColumns(10);
		tfReportViewHaemoglobin.setBounds(283, 354, 169, 20);
		tfReportViewHaemoglobin.setEditable(false);
		tfReportViewHaemoglobin.setText(String.valueOf(reportDetail.getReportHaemoglobin()));
		contentPane.add(tfReportViewHaemoglobin);
		
		tfReportViewWBC = new JTextField();
		tfReportViewWBC.setColumns(10);
		tfReportViewWBC.setBounds(283, 387, 169, 20);
		tfReportViewWBC.setEditable(false);
		tfReportViewWBC.setText(String.valueOf(reportDetail.getReportWBC()));
		contentPane.add(tfReportViewWBC);
		
		tfReportViewRBC = new JTextField();
		tfReportViewRBC.setColumns(10);
		tfReportViewRBC.setBounds(283, 418, 169, 20);
		tfReportViewRBC.setEditable(false);
		tfReportViewRBC.setText(String.valueOf(reportDetail.getReportRBC()));
		contentPane.add(tfReportViewRBC);
		
		tfReportViewPlatelet = new JTextField();
		tfReportViewPlatelet.setColumns(10);
		tfReportViewPlatelet.setBounds(283, 449, 169, 20);
		tfReportViewPlatelet.setEditable(false);
		tfReportViewPlatelet.setText(String.valueOf(reportDetail.getReportPlatelet()));
		contentPane.add(tfReportViewPlatelet);
		
		tfReportViewBodyTemperatue = new JTextField();
		tfReportViewBodyTemperatue.setColumns(10);
		tfReportViewBodyTemperatue.setBounds(283, 475, 169, 20);
		tfReportViewBodyTemperatue.setEditable(false);
		tfReportViewBodyTemperatue.setText(String.valueOf(reportDetail.getReportTemperatureF()));
		
		contentPane.add(tfReportViewBodyTemperatue);
		
		tfReportViewVision = new JTextField();
		tfReportViewVision.setColumns(10);
		tfReportViewVision.setBounds(283, 513, 169, 20);
		tfReportViewVision.setEditable(false);
		tfReportViewVision.setText(reportDetail.getReportVision());
		contentPane.add(tfReportViewVision);
		
		tfReportViewSystolic = new JTextField();
		tfReportViewSystolic.setColumns(10);
		tfReportViewSystolic.setBounds(283, 541, 169, 20);
		tfReportViewSystolic.setEditable(false);
		tfReportViewSystolic.setText(String.valueOf(reportDetail.getReportBloodSystolic()));
		contentPane.add(tfReportViewSystolic);
		
		tfReportViewDiastolic = new JTextField();
		tfReportViewDiastolic.setColumns(10);
		tfReportViewDiastolic.setBounds(283, 569, 169, 20);
		tfReportViewDiastolic.setEditable(false);
		tfReportViewDiastolic.setText(String.valueOf(reportDetail.getReportBloodDiastolic()));
		contentPane.add(tfReportViewDiastolic);
		
		tfReportViewRespiratoryRate = new JTextField();
		tfReportViewRespiratoryRate.setColumns(10);
		tfReportViewRespiratoryRate.setBounds(283, 600, 169, 20);
		tfReportViewRespiratoryRate.setEditable(false);
		tfReportViewRespiratoryRate.setText(String.valueOf(reportDetail.getReportRespiratoryRate()));
		
		contentPane.add(tfReportViewRespiratoryRate);
		
		tfReportViewResult = new JTextField();
		tfReportViewResult.setFont(new Font("Footlight MT Light", Font.PLAIN, 15));
		tfReportViewResult.setColumns(10);
		tfReportViewResult.setBounds(283, 631, 169, 20);
		tfReportViewResult.setEditable(false);
		tfReportViewResult.setText(reportDetail.getReportResult());
		contentPane.add(tfReportViewResult);
		
		tfReportViewDoctorId = new JTextField();
		tfReportViewDoctorId.setColumns(10);
		tfReportViewDoctorId.setBounds(283, 45, 169, 20);
		tfReportViewDoctorId.setEditable(false);
		tfReportViewDoctorId.setText(String.valueOf(reportDetail.getReportDoctorId()));
		contentPane.add(tfReportViewDoctorId);
		
		tfReportViewPatientId = new JTextField();
		tfReportViewPatientId.setColumns(10);
		tfReportViewPatientId.setBounds(283, 76, 169, 20);
		tfReportViewPatientId.setEditable(false);
		tfReportViewPatientId.setText(String.valueOf(reportDetail.getReportPatientId()));
		contentPane.add(tfReportViewPatientId);
		
		tfReportViewReportType = new JTextField();
		tfReportViewReportType.setText("0");
		tfReportViewReportType.setEditable(false);
		tfReportViewReportType.setColumns(10);
		tfReportViewReportType.setBounds(283, 188, 169, 20);
		tfReportViewReportType.setText(reportDetail.getReportType());
		contentPane.add(tfReportViewReportType);
		
		tfReportViewBloodGroup = new JTextField();
		tfReportViewBloodGroup.setText("0");
		tfReportViewBloodGroup.setEditable(false);
		tfReportViewBloodGroup.setColumns(10);
		tfReportViewBloodGroup.setBounds(283, 323, 169, 20);
		tfReportViewReportType.setText(reportDetail.getReportBloodGroup());
		contentPane.add(tfReportViewBloodGroup);
	}
}