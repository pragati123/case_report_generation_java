package com.crfg.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.crfg.model.FeedbackDetail;
import com.crfg.service.FeedbackService;

@SuppressWarnings("serial")
public class Feedback extends JFrame {

	private JPanel contentPane;
	private JTextField tfFeedbackId;
	private JComboBox<String> cbRole;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Feedback frame = new Feedback();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Feedback() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 528);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFeedbackForm = new JLabel("FEEDBACK");
		lblFeedbackForm.setFont(new Font("Footlight MT Light", Font.PLAIN, 22));
		lblFeedbackForm.setBounds(176, 11, 107, 36);
		contentPane.add(lblFeedbackForm);
		
		JLabel lblRole = new JLabel("Role");
		lblRole.setFont(new Font("Footlight MT Light", Font.PLAIN, 18));
		lblRole.setBounds(10, 54, 107, 27);
		contentPane.add(lblRole);
		
		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Footlight MT Light", Font.PLAIN, 18));
		lblId.setBounds(10, 92, 107, 27);
		contentPane.add(lblId);
		
		tfFeedbackId = new JTextField();
		tfFeedbackId.setColumns(10);
		tfFeedbackId.setBounds(146, 100, 86, 20);
		contentPane.add(tfFeedbackId);
		
		cbRole = new JComboBox<String>();
		
		cbRole.setBounds(146, 58, 86, 20);
		cbRole.addItem("Doctor");
		cbRole.addItem("Patient");
		contentPane.add(cbRole);
		
		JLabel lblFeedback = new JLabel("FeedBack");
		lblFeedback.setFont(new Font("Footlight MT Light", Font.PLAIN, 18));
		lblFeedback.setBounds(10, 130, 107, 27);
		contentPane.add(lblFeedback);
		
		JTextArea taFeedback = new JTextArea();
		taFeedback.setBounds(10, 168, 414, 239);
		contentPane.add(taFeedback);
		
		JButton btnSubmit = new JButton("SUBMIT");
		btnSubmit.setBounds(191, 437, 89, 23);
		contentPane.add(btnSubmit);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s=(String)cbRole.getSelectedItem();
				FeedbackDetail feedbackDetail = new FeedbackDetail();
				
				feedbackDetail.setFeedbackRole(s);
				feedbackDetail.setFeedbackId(Integer.parseInt(tfFeedbackId.getText()));
				
			feedbackDetail.setFeedback(taFeedback.getText());
				
			FeedbackService feedbackService=new FeedbackService();
			int x=feedbackService.addFeedbackService(feedbackDetail);
			if(x>0){
				System.out.print("Connected");
			}
			}
		});
	}
}