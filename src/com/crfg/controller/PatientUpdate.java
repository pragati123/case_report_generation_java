package com.crfg.controller;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.crfg.service.PatientService;
import java.awt.Color;

@SuppressWarnings("serial")
public class PatientUpdate extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PatientUpdate frame = new PatientUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PatientUpdate() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 468, 361);
		JPanel contentPane=new JPanel();
		contentPane.setBackground(new Color(153, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPatientProfileUpdate = new JLabel(" Update Patient Profile");
		lblPatientProfileUpdate.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		lblPatientProfileUpdate.setBounds(106, 32, 233, 29);
		contentPane.add(lblPatientProfileUpdate);
		
		JLabel lblPatientId = new JLabel("Patient ID");
		lblPatientId.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblPatientId.setBounds(24, 91, 127, 14);
		getContentPane().add(lblPatientId);
		
		JTextField tfPatientId = new JTextField();
		tfPatientId.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfPatientId.setBounds(235, 88, 86, 20);
		getContentPane().add(tfPatientId);
		tfPatientId.setColumns(10);
		
		JLabel lblUpdateField = new JLabel("What to Update ?");
		lblUpdateField.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblUpdateField.setBounds(24, 123, 127, 37);
		contentPane.add(lblUpdateField);
		
		JComboBox<String> cbUpdateField = new JComboBox<String>();
		cbUpdateField.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		cbUpdateField.setBounds(235, 131, 86, 20);
		contentPane.add(cbUpdateField);
		cbUpdateField.addItem("Patient Name");
		cbUpdateField.addItem("Patient Age");
		cbUpdateField.addItem("Patient Gender");
		cbUpdateField.addItem("Patient Address");
		cbUpdateField.addItem("Patient Contact");
		cbUpdateField.addItem("Patient Email");
		cbUpdateField.addItem("Patient Username");
		cbUpdateField.addItem("Patient Password");
				
		JLabel lblUpdatedData = new JLabel("Update the Selected Field:");
		lblUpdatedData.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblUpdatedData.setBounds(24, 182, 201, 17);
		contentPane.add(lblUpdatedData);
		
		JTextField tfUpdatedData = new JTextField();
		tfUpdatedData.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfUpdatedData.setBounds(235, 180, 86, 20);
		contentPane.add(tfUpdatedData);
		tfUpdatedData.setColumns(10);
		
		JButton btnPatientUpdate = new JButton("Update");
		btnPatientUpdate.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnPatientUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				PatientService patientService=new PatientService();
				int patientId=Integer.parseInt(tfPatientId.getText());
				String updatedData=tfUpdatedData.getText();
				System.out.println(cbUpdateField.getSelectedItem());
				if(cbUpdateField.getSelectedItem().equals("Patient Name")){
					try{
						int x=patientService.updatePatientName(updatedData, patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Age")){
					try{
						int x=patientService.updatePatientAge(Integer.parseInt(updatedData), patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Address")){
					try{
						int x=patientService.updatePatientAddress(updatedData, patientId);
						if(x>0){
							System.out.println("Up Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Gender")){
					try{
						int x=patientService.updatePatientGender(updatedData, patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Contact")){
					try{
						int x=patientService.updatePatientContact(Integer.parseInt(updatedData), patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Email")){
					try{
						int x=patientService.updatePatientEmail(updatedData, patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Username")){
					try{
						int x=patientService.updatePatientUsername(updatedData, patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Patient Password")){
					try{
						int x=patientService.updatePatientPassword(updatedData, patientId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnPatientUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(btnPatientUpdate, "All fields are mandotory "+ex);
	                } 
				}
			}
		});
		btnPatientUpdate.setBounds(174, 235, 89, 23);
		contentPane.add(btnPatientUpdate);
		
		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Comic Sans MS", Font.PLAIN, 11));
		btnBack.setBounds(7, 11, 89, 23);
		contentPane.add(btnBack);
	}
}