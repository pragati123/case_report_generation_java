package com.crfg.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;

import com.crfg.model.DoctorDetail;
import com.crfg.service.PatientService;
import java.awt.Color;

@SuppressWarnings("serial")
public class DoctorView extends JFrame {

	private JPanel contentPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DoctorView frame = new DoctorView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DoctorView() {
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 648, 473);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 204, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGetDoctorDetail = new JLabel("Get Doctor Detail");
		lblGetDoctorDetail.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		lblGetDoctorDetail.setBounds(170, 35, 177, 21);
		contentPane.add(lblGetDoctorDetail);
		
		JLabel lblDoctorName = new JLabel("Doctor Name");
		lblDoctorName.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorName.setBounds(71, 122, 150, 14);
		contentPane.add(lblDoctorName);
		
		JLabel lblDoctorQualification = new JLabel("Doctor Qualifiaction");
		lblDoctorQualification.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorQualification.setBounds(71, 153, 150, 17);
		contentPane.add(lblDoctorQualification);
		
		JLabel lblDoctorSpecialization = new JLabel("Doctor Specialization");
		lblDoctorSpecialization.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorSpecialization.setBounds(71, 84, 150, 27);
		contentPane.add(lblDoctorSpecialization);
		
		JComboBox<String> cbSpecialization = new JComboBox<String>();
		cbSpecialization.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		cbSpecialization.setBounds(284, 84, 150, 20);
		contentPane.add(cbSpecialization);
		
		cbSpecialization.addItem("Radiology");
		cbSpecialization.addItem("Cardio");
		
		
		JComboBox<String> cbDoctorNames = new JComboBox<String>();
		cbDoctorNames.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		cbDoctorNames.setBounds(284, 119, 150, 20);
		contentPane.add(cbDoctorNames);
		
		
		JTextField tfDoctorQualification = new JTextField();
		tfDoctorQualification.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorQualification.setBounds(284, 151, 150, 20);
		contentPane.add(tfDoctorQualification);
		tfDoctorQualification.setColumns(10);
		
		JTextArea taDoctorAddress = new JTextArea();
		taDoctorAddress.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		taDoctorAddress.setBounds(283, 243, 151, 45);
		contentPane.add(taDoctorAddress);
		
		JLabel lblDoctorWorkExperience = new JLabel("Doctor Work Experience");
		lblDoctorWorkExperience.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorWorkExperience.setBounds(73, 181, 201, 14);
		contentPane.add(lblDoctorWorkExperience);
		
		JTextField tfDoctorWorkExperience = new JTextField();
		tfDoctorWorkExperience.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorWorkExperience.setBounds(284, 182, 150, 20);
		contentPane.add(tfDoctorWorkExperience);
		tfDoctorWorkExperience.setColumns(10);
		
		JLabel lblDoctorFees = new JLabel("Doctor Fees");
		lblDoctorFees.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorFees.setBounds(73, 206, 165, 14);
		contentPane.add(lblDoctorFees);
		
		JLabel lblDoctorAddress = new JLabel("Doctor Address");
		lblDoctorAddress.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorAddress.setBounds(71, 249, 137, 14);
		contentPane.add(lblDoctorAddress);
		
		JTextField tfDoctorFees = new JTextField();
		tfDoctorFees.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorFees.setBounds(283, 213, 151, 20);
		contentPane.add(tfDoctorFees);
		tfDoctorFees.setColumns(10);
		
		JLabel lblDoctorConatct = new JLabel("Doctor Contact");
		lblDoctorConatct.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorConatct.setBounds(71, 343, 150, 14);
		contentPane.add(lblDoctorConatct);
		
		JTextField tfDoctorContact = new JTextField();
		tfDoctorContact.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorContact.setBounds(283, 340, 151, 20);
		contentPane.add(tfDoctorContact);
		tfDoctorContact.setColumns(10);
		
		JLabel lblDoctorTiming = new JLabel("Doctor Timing");
		lblDoctorTiming.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorTiming.setBounds(72, 312, 136, 20);
		contentPane.add(lblDoctorTiming);
		
		JTextField tfDoctorTiming = new JTextField();
		tfDoctorTiming.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorTiming.setBounds(283, 309, 151, 20);
		contentPane.add(tfDoctorTiming);
		tfDoctorTiming.setColumns(10);
		
		JTextField tfDoctorEmail = new JTextField();
		tfDoctorEmail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfDoctorEmail.setBounds(283, 371, 151, 20);
		contentPane.add(tfDoctorEmail);
		tfDoctorEmail.setColumns(10);
		
		JLabel lblDoctorEmail = new JLabel("Doctor Email");
		lblDoctorEmail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblDoctorEmail.setBounds(71, 375, 118, 14);
		contentPane.add(lblDoctorEmail);
		
		JButton btnGetDoctorNames = new JButton("Get Doctor Names");
		btnGetDoctorNames.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnGetDoctorNames.setBounds(444, 81, 176, 23);
		contentPane.add(btnGetDoctorNames);
		btnGetDoctorNames.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> doctorNames=new ArrayList<String>();
				PatientService patientService = new PatientService();
				String doctorSpecialization=(String) cbSpecialization.getSelectedItem();
						doctorNames=patientService.getDoctorNames(doctorSpecialization);
						for (int i = 0; i < doctorNames.size(); i++) {
							cbDoctorNames.addItem(doctorNames.get(i));
							}	
					} 
		});
		
		JButton btnGetDetail = new JButton("Get Detail");
		btnGetDetail.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnGetDetail.setBounds(444, 118, 176, 23);
		contentPane.add(btnGetDetail);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PatientDashboard patientDashboard=new PatientDashboard ();
				patientDashboard.setVisible(true);
				dispose();
				
			}
		});
		btnBack.setBounds(36, 22, 89, 23);
		contentPane.add(btnBack);
		btnGetDetail.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent){
				PatientService patientService=new PatientService();
				DoctorDetail doctorDetail=new DoctorDetail();
				String doctorName = (String)cbDoctorNames.getSelectedItem();
				doctorDetail=patientService.getDoctorDetail(doctorName);
				
				tfDoctorQualification.setText(doctorDetail.getDoctorQualification());
				tfDoctorWorkExperience.setText(doctorDetail.getDoctorWorkExperience());
				tfDoctorFees.setText(Float.toString(doctorDetail.getDoctorFees()));
				taDoctorAddress.setText(doctorDetail.getDoctorAddress());
				tfDoctorContact.setText(Long.toString(doctorDetail.getDoctorContact()));
				tfDoctorTiming.setText(doctorDetail.getDoctorTiming());
				tfDoctorEmail.setText(doctorDetail.getDoctorEmail());
					} 
				}
		);
			}
}