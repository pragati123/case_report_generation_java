package com.crfg.controller;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.crfg.service.PatientService;

@SuppressWarnings("serial")
public class PatientLogin extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PatientLogin frame = new PatientLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PatientLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 441, 434);
		JPanel contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPatientLogin = new JLabel("Patient Login");
		lblPatientLogin.setForeground(Color.RED);
		lblPatientLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		lblPatientLogin.setBackground(Color.WHITE);
		lblPatientLogin.setBounds(172, 75, 140, 40);
		contentPane.add(lblPatientLogin);
		
		JLabel lblPatientUsername = new JLabel("User name");
		lblPatientUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		lblPatientUsername.setBounds(102, 137, 93, 25);
		contentPane.add(lblPatientUsername);
		
		JTextField tfPatientUsername = new JTextField();
		tfPatientUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		tfPatientUsername.setColumns(10);
		tfPatientUsername.setBounds(228, 139, 115, 20);
		contentPane.add(tfPatientUsername);
		
		JLabel lblPatientPassword = new JLabel("Password");
		lblPatientPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		lblPatientPassword.setBounds(102, 185, 74, 25);
		contentPane.add(lblPatientPassword);
		
		JPasswordField pfPatientPassword = new JPasswordField();
		pfPatientPassword.setBounds(228, 189, 115, 20);
		contentPane.add(pfPatientPassword);
		
		JButton btnPatientLogin = new JButton("Login");
		btnPatientLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnPatientLogin.setBounds(177, 244, 93, 35);
		contentPane.add(btnPatientLogin);
		btnPatientLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				PatientService patientService=new PatientService();
				String username=tfPatientUsername.getText();
				char[] password = pfPatientPassword.getPassword();
				String passwordString = new String(password);
				boolean validate=patientService.validateDoctor(username,passwordString);
				if(validate){
					JOptionPane.showMessageDialog(btnPatientLogin,"You have successfully logged in");
					PatientDashboard patientDashboard = new PatientDashboard();
					patientDashboard.setVisible(true);
					dispose();
					
				}
				else{
					JOptionPane.showMessageDialog(btnPatientLogin,"You are not valid user!");
					
				}
			}
		});
		
		
		JLabel lblPatientNotamemeberyet = new JLabel("Not a memeber yet??");
		lblPatientNotamemeberyet.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		lblPatientNotamemeberyet.setBounds(80, 318, 150, 25);
		contentPane.add(lblPatientNotamemeberyet);
		
		JButton btnPatientRegister = new JButton("Register Here");
		btnPatientRegister.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnPatientRegister.setBounds(240, 319, 146, 40);
		contentPane.add(btnPatientRegister);
	
		btnPatientRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				PatientRegistration patientRegistration = new PatientRegistration();
				patientRegistration.setVisible(true);
				dispose();
				}
		}); 
		}
}