package com.crfg.controller;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JTabbedPane;
import javax.swing.JTable;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JScrollPane;

import com.crfg.model.FeedbackDetail;
import com.crfg.service.DoctorService;
import com.crfg.service.FeedbackService;


import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")

public class DoctorDashboard extends JFrame{
	
	private JPanel contentPane;
	private JTextField tfReportSrNo;
	private JTable reportTable;
	private JTextField tfDoctorId;
	private JTextField tfAppointmentSrNo;
	private JTable appointmentTable;
	private JTextField tfUpdatedData;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DoctorDashboard frame = new DoctorDashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DoctorDashboard() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DoctorDashboard.class.getResource("/com/crfg/controller/pics/medical_cross_icon_cg1p74225010c_th.jpg")));
		setForeground(Color.WHITE);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1440, 750);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 51));
		contentPane.setForeground(new Color(102, 204, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDoctorDashboard = new JLabel("Doctor Dashboard");
		lblDoctorDashboard.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblDoctorDashboard.setBounds(447, 18, 379, 114);
		contentPane.add(lblDoctorDashboard);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 119, 1362, 592);
		contentPane.add(tabbedPane);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(255, 204, 51));
		tabbedPane.addTab("Feedback", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblRole = new JLabel("Role");
		lblRole.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblRole.setBounds(433, 95, 107, 27);
		panel_3.add(lblRole);
		
		JLabel lblFeedback = new JLabel("FeedBack");
		lblFeedback.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		lblFeedback.setBounds(433, 148, 107, 27);
		panel_3.add(lblFeedback);
		
		JTextArea taFeedback = new JTextArea();
		taFeedback.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		taFeedback.setBounds(598, 144, 414, 247);
		panel_3.add(taFeedback);
		
		JComboBox<String> cbRole = new JComboBox<String>();
		cbRole.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		cbRole.setBounds(598, 96, 164, 37);
		panel_3.add(cbRole);
		
		cbRole.addItem("Doctor");
		cbRole.addItem("Patient");
		
		JButton btnSubmit = new JButton("SUBMIT");
		btnSubmit.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnSubmit.setBounds(685, 412, 117, 23);
		panel_3.add(btnSubmit);
		
		tfDoctorId = new JTextField();
		tfDoctorId.setBounds(145, 68, 157, 20);
		contentPane.add(tfDoctorId);
		tfDoctorId.setText("Enter Doctor Id");
		tfDoctorId.setColumns(10);
		
		
		JLabel label_5 = new JLabel("");
		label_5.setBackground(new Color(0, 102, 153));
		label_5.setIcon(new ImageIcon(DoctorDashboard.class.getResource("/com/crfg/controller/pics/tumblr_mcuedfNz9e1qbe7iu.png")));
		label_5.setBounds(20, 186, 435, 312);
		panel_3.add(label_5);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s=(String)cbRole.getSelectedItem();
				FeedbackDetail feedbackDetail = new FeedbackDetail();
				String doctorId=tfDoctorId.getText();
				
				feedbackDetail.setFeedbackRole(s);
				feedbackDetail.setFeedbackId(Integer.parseInt(doctorId));
				
			feedbackDetail.setFeedback(taFeedback.getText());
			
			FeedbackService feedbackService=new FeedbackService();
			int x=feedbackService.addFeedbackService(feedbackDetail);
			if(x>0){
				JOptionPane.showMessageDialog(null, "Thanks for giving Feedback. Have a nice day");
			}
			
		
		}
	});
		
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 204, 51));
		tabbedPane.addTab("Report", null, panel, null);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 33, 1345, 402);
		panel.add(scrollPane);
		
		reportTable = new JTable();
		scrollPane.setViewportView(reportTable);
		reportTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRowIndex = reportTable.getSelectedRow();
				tfReportSrNo.setText(reportTable.getValueAt(selectedRowIndex, 0).toString());

			}
		});
		
		tfReportSrNo = new JTextField();
		tfReportSrNo.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfReportSrNo.setText("           Report Sr No");
		tfReportSrNo.setBounds(363, 507, 116, 23);
		panel.add(tfReportSrNo);
		tfReportSrNo.setColumns(10);
		
		JButton btnViewReport = new JButton("View");
		btnViewReport.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnViewReport.setBounds(12, 433, 1345, 52);
		panel.add(btnViewReport);
		btnViewReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorService doctorService = new DoctorService();
				int doctorId =Integer.parseInt(tfDoctorId.getText());
				doctorService.viewReport(reportTable,doctorId);
				}
		});
		
		JButton btnDeleteReport = new JButton("Delete");
		btnDeleteReport.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDeleteReport.setBounds(556, 504, 86, 29);
		panel.add(btnDeleteReport);
		btnDeleteReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorService doctorService = new DoctorService();
				int serialNumber =Integer.parseInt(tfReportSrNo.getText());
				doctorService.deleteReport(serialNumber);
			}
		});
		
		JButton btnGenerateReport = new JButton("Generate Report");
		btnGenerateReport.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnGenerateReport.setBounds(12, 496, 317, 45);
		panel.add(btnGenerateReport);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 204, 51));
		tabbedPane.addTab("Update", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel label = new JLabel("Update Doctor Profile");
		label.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		label.setBounds(338, 148, 234, 37);
		panel_2.add(label);
		
		JLabel label_1 = new JLabel("What to Update?");
		label_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		label_1.setBounds(233, 228, 158, 37);
		panel_2.add(label_1);
		
		JComboBox<String> cbUpdateField = new JComboBox<String>();
		cbUpdateField.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		cbUpdateField.setBounds(477, 226, 170, 39);
		panel_2.add(cbUpdateField);
		
		cbUpdateField.addItem("Name");
		cbUpdateField.addItem("Qualification");
		cbUpdateField.addItem("Specialization");
		cbUpdateField.addItem("Work Experince");
		cbUpdateField.addItem("Fees");
		cbUpdateField.addItem("Address");
		cbUpdateField.addItem("Contact");
		cbUpdateField.addItem("Timing");
		cbUpdateField.addItem("Username");
		cbUpdateField.addItem("Password");
		cbUpdateField.addItem("Email");		
		
		JLabel label_4 = new JLabel("Update the Selected Field ");
		label_4.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		label_4.setBounds(233, 301, 246, 37);
		panel_2.add(label_4);
		
		tfUpdatedData = new JTextField();
		tfUpdatedData.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfUpdatedData.setColumns(10);
		tfUpdatedData.setBounds(477, 288, 170, 37);
		panel_2.add(tfUpdatedData);
		
		JButton btnDoctorUpdate = new JButton("Update");
		btnDoctorUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorService doctorService=new DoctorService();
				int doctorId=Integer.parseInt(tfDoctorId.getText());
				String updatedData=tfUpdatedData.getText();
				System.out.println(cbUpdateField.getSelectedItem());
				if(cbUpdateField.getSelectedItem().equals("Name")){
					try{
						int x=doctorService.updateDoctorName(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Qualification")){
					try{
						int x=doctorService.updateDoctorQualification(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(null,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Specialization")){
					try{
						int x=doctorService.updateDoctorSpecialization(updatedData, doctorId);
						if(x>0){
							System.out.println("Up Successfully");
							JOptionPane.showMessageDialog(null,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory "+ex);
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Work Experience")){
					try{
						int x=doctorService.updateDoctorWorkExperience(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(null,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Fees")){
					try{
						int x=doctorService.updateDoctorFees(Integer.parseInt(updatedData), doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Address")){
					try{
						int x=doctorService.updateDoctorAddress(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Contact")){
					try{
						int x=(int) doctorService.updateDoctorContact(Long.parseLong(updatedData), doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Username")){
					try{
						int x=doctorService.updateDoctorUsername(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Email")){
					try{
						int x=doctorService.updateDoctorEmail(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}
				else if(cbUpdateField.getSelectedItem().equals("Password")){
					try{
						int x=doctorService.updateDoctorPassword(updatedData, doctorId);
						if(x>0){
							System.out.println("Updated Successfully");
							JOptionPane.showMessageDialog(btnDoctorUpdate,"Your Data updated Successfully!");
						}}
					catch (NumberFormatException ex) 
	                {
	        	  JOptionPane.showMessageDialog(null, "All fields are mandotory ");
	                } 
				}

			}
		});
		btnDoctorUpdate.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDoctorUpdate.setBounds(368, 363, 111, 37);
		panel_2.add(btnDoctorUpdate);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 204, 51));
		tabbedPane.addTab("Appointment", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 56, 1357, 328);
		panel_1.add(scrollPane_1);
		
		appointmentTable = new JTable();
		scrollPane_1.setViewportView(appointmentTable);
		
		tfAppointmentSrNo = new JTextField();
		tfAppointmentSrNo.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		tfAppointmentSrNo.setText("Appointment Sr No");
		tfAppointmentSrNo.setBounds(507, 481, 163, 45);
		panel_1.add(tfAppointmentSrNo);
		tfAppointmentSrNo.setColumns(10);
		
		appointmentTable = new JTable();
		scrollPane_1.setViewportView(appointmentTable);
		appointmentTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRowIndex = appointmentTable.getSelectedRow();
				tfAppointmentSrNo.setText(appointmentTable.getValueAt(selectedRowIndex, 0).toString());

			}
		});
		
		JButton btnViewAppointment = new JButton("View");
		btnViewAppointment.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnViewAppointment.setBounds(0, 383, 1357, 53);
		panel_1.add(btnViewAppointment);
		btnViewAppointment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorService doctorService = new DoctorService();
				int doctorId =Integer.parseInt(tfDoctorId.getText());
				doctorService.viewAppointment(appointmentTable,doctorId);
			}
		});
		
		JButton btnDeleteAppointment = new JButton("Delete");
		btnDeleteAppointment.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDeleteAppointment.setBounds(739, 486, 110, 34);
		panel_1.add(btnDeleteAppointment);
		
		JLabel lblAppointments = new JLabel("Appointments");
		lblAppointments.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblAppointments.setBounds(528, 0, 205, 53);
		panel_1.add(lblAppointments);
		btnDeleteAppointment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorService doctorService = new DoctorService();
				int serialNumber =Integer.parseInt(tfAppointmentSrNo.getText());
				doctorService.deleteAppointment(serialNumber);
			}
		});
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(DoctorDashboard.class.getResource("/com/crfg/controller/pics/profile-icon.png")));
		label_2.setBounds(0, -9, 144, 141);
		contentPane.add(label_2);
		
		JButton button_1 = new JButton("");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage1 homePage1=new HomePage1();
				homePage1.setVisible(true);
				dispose();
				
			}
		});
		button_1.setIcon(new ImageIcon(DoctorDashboard.class.getResource("/com/crfg/controller/pics/Apps-session-logout-icon.png")));
		button_1.setBounds(1143, 18, 98, 91);
		contentPane.add(button_1);
		btnGenerateReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReportGeneration reportGeneration=new ReportGeneration();
				reportGeneration.setVisible(true);

			}
		});
	}

}
