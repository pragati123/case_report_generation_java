package com.crfg.service;

import java.util.List;

import javax.swing.JTable;

import com.crfg.dao.DoctorDAO;
import com.crfg.model.DoctorDetail;

public class DoctorService {
	
	DoctorDAO doctorDao;
	public DoctorService()
	{
		doctorDao=new DoctorDAO();
	}
	
	public int addDoctorService(DoctorDetail doctorDetail) {
		return doctorDao.addDoctor(doctorDetail);
		}
	
	public boolean vaidateDoctor(String username,String passwordString){
		return doctorDao.validateDoctor(username,passwordString);
	}
	
	public List<String> getDoctorName(String doctorSpecialization){
		return doctorDao.getDoctorName(doctorSpecialization);
	}
	
	public int getDoctorId(String doctorName){
		return doctorDao.getDoctorId(doctorName);
	}
	
	public int updateDoctorName(String doctorName, int doctorId){
		return doctorDao.updateDoctorName(doctorName,doctorId);
	}
	
	public int updateDoctorQualification(String doctorQualification, int doctorId){
		return doctorDao.updateDoctorQualification(doctorQualification,doctorId);
	}
	
	public int updateDoctorSpecialization(String doctorSpecialization, int doctorId){
		return doctorDao.updateDoctorSpecialization(doctorSpecialization,doctorId);
	}
	
	public int updateDoctorWorkExperience(String doctorWorkExperience, int doctorId){
		return doctorDao.updateDoctorWorkExperience(doctorWorkExperience,doctorId);
	}
	
	public int updateDoctorFees(float doctorFees, int doctorId){
		return doctorDao.updateDoctorFees(doctorFees,doctorId);
	}

	public int updateDoctorAddress(String doctorAddress, int doctorId){
		return doctorDao.updateDoctorAddress(doctorAddress,doctorId);
	}

	public long updateDoctorContact(long doctorContact, int doctorId){
		return doctorDao.updateDoctorContact(doctorContact,doctorId);
	}
	
	public int updateDoctorTiming(String doctorTiming, int doctorId){
		return doctorDao.updateDoctorTiming(doctorTiming,doctorId);
	}
	
	public int updateDoctorEmail(String doctorEmail, int doctorId){
		return doctorDao.updateDoctorEmail(doctorEmail,doctorId);
	}
	
	public int updateDoctorUsername(String doctorUsername, int doctorId){
		return doctorDao.updateDoctorUsername(doctorUsername,doctorId);
	}
	
	public int updateDoctorPassword(String doctorPassword, int doctorId){
		return doctorDao.updateDoctorPassword(doctorPassword,doctorId);
	}
	
	public void viewReport(JTable reportTable,int doctorId) {
		doctorDao.viewReport(reportTable,doctorId);
	}
	
	public void deleteReport(int serialNumber) {
		doctorDao.deleteReport(serialNumber);
	}
	
	public void viewAppointment(JTable appointmentTable, int doctorId) {
		doctorDao.viewAppointment(appointmentTable,doctorId);
	}
	
	public void deleteAppointment(int serialNumber) {
		doctorDao.deleteAppointment(serialNumber);
		
	}

}
