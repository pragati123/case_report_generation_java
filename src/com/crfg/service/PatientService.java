package com.crfg.service;

import java.util.List;

import javax.swing.JTable;

import com.crfg.dao.PatientDAO;
import com.crfg.model.DoctorDetail;
import com.crfg.model.PatientDetail;

public class PatientService {
	PatientDAO patientDao;
	public PatientService(){
		patientDao= new PatientDAO();
	}
	
	public int addPatientService(PatientDetail patientdetail){
		return patientDao.addpatient(patientdetail);
	}
	
	public boolean validateDoctor(String username, String passwordString) {
		return patientDao.validatePatient(username, passwordString);
	}
	
	public int updatePatientName(String patientName, int patientId){
		return patientDao.updatePatientName(patientName, patientId);
	}
	
	public int updatePatientAge(int patientAge, int patientId){
		return patientDao.updatePatientAge(patientAge, patientId);
	}
	
	public int updatePatientAddress(String patientAddress, int patientId){
		return patientDao.updatePatientAddress(patientAddress, patientId);
	}
	
	public int updatePatientGender(String patientGender,int patientId){
		return patientDao.updatePatientAddress(patientGender, patientId);
	}
	
	public int updatePatientContact(int patientContact,int patientId){
		return patientDao.updatePatientContact(patientContact, patientId);
	}
	
	public int updatePatientEmail(String patientEmail,int patientId){
		return patientDao.updatePatientEmail(patientEmail, patientId);
	}
	
	public int updatePatientUsername(String patientUsername,int patientId){
		return patientDao.updatePatientUsername(patientUsername, patientId);
	}
	
	public int updatePatientPassword(String password, int patientId){
		return patientDao.updatePatientPassword(password, patientId);
	}
	
	public List<String> getDoctorNames(String doctorSpecialization) {
		return patientDao.getDoctorNames(doctorSpecialization);		
	}
	public DoctorDetail getDoctorDetail(String doctorName) {
		return patientDao.getDoctorDetail(doctorName);
		
	}
	public void viewGeneratedReport(JTable myReportsTable, int patientId) {
		patientDao.viewGeneratedReport(myReportsTable,patientId);
		
	}

}
