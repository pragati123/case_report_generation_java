package com.crfg.service;

import javax.swing.JTable;

import com.crfg.dao.AdminDAO;

public class AdminService {
	
AdminDAO adminDao;
	
	public AdminService(){
		adminDao=new AdminDAO();
	}
	public boolean validateAdmin(String username,String passwordString){
		return adminDao.validateAdmin(username, passwordString);
	}
	public void displayDoctor(JTable doctorTable) {
		adminDao.displayDoctor(doctorTable);	
	}
	public void displayPatient(JTable patientTable) {
		adminDao.displayPatient(patientTable);	
	}
	public void displayFeedback(JTable feedbackTable) {
		adminDao.displayFeedback(feedbackTable);
	}
	public void deleteDoctorDetail(int doctorId) {
		adminDao.deleteDoctorDetail(doctorId);
	}

}
